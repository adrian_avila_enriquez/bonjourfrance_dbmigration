<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  <style>
		.grab{cursor:move;}
		.draggable{overflow:hidden;}
    .droppable { background-color: grey; }
		.hovered{ background-color: #eee; }
		.correct{ background-color: green; }
		.incorrect{ background-color: red; }
		.element{min-height:150px;max-height:150px;}
		.image-link{cursor:zoom-in;}
		* {
		  border-radius: 0 !important;
		}
  </style>

</head>
<body>

  <div class="container">

		<div class="page-header">
			<h1>Association deux colonnes VIDEOS / TEXTES</h1>
		</div>

		<div class="alert alert-info" role="alert">
			<p><strong>Regardez les extraits vidéos</strong> en cliquant dessus. <strong>Déplacer</strong> ensuite les proposition de la colonne à droit en cliquant sur l'icone <i class="fa fa-arrows"></i> et en la faisant <strong>glisser</strong> sur la zone grise.</p>
		</div>

		<div class="row">
			<div class="col-md-9">
				<div class="row">
		      <div class="col-md-8">

						<div class="row">
							<div class="col-md-6 element">
								<div class="panel element panel-default">
									<iframe width="100%" height="150" src="https://www.youtube.com/embed/1PhrvI6ecRs" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>
							<div data-identifier="1" class="droppable col-md-6 element"></div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<iframe frameborder="0" width="100%" height="150" src="//www.dailymotion.com/embed/video/x2tgqvc" allowfullscreen></iframe>
								</div>
							</div>
							<div data-identifier="2" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<iframe frameborder="0" width="100%" height="150" src="//www.dailymotion.com/embed/video/x2ptioi" allowfullscreen></iframe>
								</div>
							</div>
							<div data-identifier="3" class="droppable col-md-6 element"></div>
						</div>

					</div>
					<div class="col-md-4">

						<div data-total="3" data-correct="1" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	<strong>Menton</strong> : La ville aux agrumes.
			        </div>
						</div>
						<div data-total="3" data-correct="2" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
								<p><strong>Nice</strong> : La ville de la Promenade des Anglais.</p>
							</div>
						</div>
						<div data-total="3" data-correct="3" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
								<p><strong>Limitations de vitesses</strong> : Le passage à 100 km/h.</p>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-3">
				<a class="btn btn-warning reset">Recommencer</a>
			</div>
		</div>

		<hr />


		<div class="page-header">
			<h1>Association deux colonnes TEXTES / IMAGES</h1>
		</div>

		<div class="row">
			<div class="col-md-9">
				<div class="row">
		      <div class="col-md-8">

						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p><strong>Menton</strong> : La ville aux agrumes.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="1" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p><strong>Nice</strong> : La ville de la Promenade des Anglais.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="2" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p><strong>Villefranche</strong> : La ville aux couleurs du Sud.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="3" class="droppable col-md-6 element"></div>
						</div>

					</div>
					<div class="col-md-4">

						<div data-total="3" data-correct="1" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/antibes_yiszil.jpg">
								<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_120,w_260/v1422979850/antibes_yiszil.jpg" alt="" />
							</a>
						</div>
						<div data-total="3" data-correct="3" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/villefranche_tokvqc.jpg">
								<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_120,w_260/v1430324306/villefranche_tokvqc.jpg" alt="" />
							</a>
						</div>
						<div data-total="3" data-correct="2" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/plage-nice_vmlflj.jpg">
								<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_120,w_260/v1430324306/plage-nice_vmlflj.jpg" alt="" />
							</a>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-3">
				<a class="btn btn-warning reset">Recommencer</a>
			</div>
		</div>

		<hr />

		<div class="page-header">
			<h1>Association deux colonnes IMAGES / TEXTES</h1>
		</div>

		<div class="row">
			<div class="col-md-9">
				<div class="row">
		      <div class="col-md-8">

						<div class="row">
							<div class="col-md-6 element">
								<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/antibes_yiszil.jpg">
									<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_150,w_260/v1422979850/antibes_yiszil.jpg" alt="" />
								</a>
							</div>
							<div data-identifier="1" class="droppable col-md-6 element"></div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-6 element">
								<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/villefranche_tokvqc.jpg">
									<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_150,w_260/v1430324306/villefranche_tokvqc.jpg" alt="" />
								</a>
							</div>
							<div data-identifier="2" class="droppable col-md-6 element"></div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-6 element">
								<a class="image-link" href="http://res.cloudinary.com/bdf/image/upload/v1422979850/plage-nice_vmlflj.jpg">
									<img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_150,w_260/v1430324306/plage-nice_vmlflj.jpg" alt="" />
								</a>
							</div>
							<div data-identifier="3" class="droppable col-md-6 element"></div>
						</div>

					</div>
					<div class="col-md-4">

						<div data-total="3" data-correct="1" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	<strong>Menton</strong> : La ville aux agrumes.
			        </div>
						</div>
						<div data-total="3" data-correct="3" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
								<strong>Nice</strong> : La ville de la Promenade des Anglais.
			        </div>
						</div>
						<div data-total="3" data-correct="2" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
								<strong>Villefranche</strong> : La ville aux couleurs du Sud.
			        </div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-3">
				<a class="btn btn-warning reset">Recommencer</a>
			</div>
		</div>

		<div class="page-header">
			<h1>Association deux colonnes TEXTES / TEXTES</h1>
		</div>

		<div class="row">
			<div class="col-md-9">
				<div class="row">
		      <div class="col-md-8">

						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>1 ) Leur objectif est de collecter d'aliments et de les mettre à disposition pour ceux qui sont dans le besoin et qui ont faim.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="1" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>2 ) Une équipe avec les nez rouges a décidé d’aller dans les hôpitaux pour améliorer la qualité des séjours des enfants malades.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="2" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>3 ) Inspirée d’un concept américain, cette émission s’est imposée au fil des ans comme un événement unique en termes de mobilisation.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="3" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6 element">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>4 ) Leur mission est de protéger la vie et la santé, en particulier en temps de conflit armé et dans d'autres situations d'urgence.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="4" class="droppable col-md-6 element"></div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-6">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>5 ) Leur mission consiste à lutter contre l’exclusion des personnes sans domicile fixe.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="5" class="droppable col-md-6 element"></div>
						</div>
						<div class="row">
							<div class="col-md-6 element">
								<div class="panel element panel-default">
									<div class="panel-body">
										<p>6 ) Mouvement de solidarité envers les plus pauvres créé par l'Abbé Pierre.</p>
					        </div>
								</div>
							</div>
							<div data-identifier="6" class="droppable col-md-6 element"></div>
						</div>

					</div>
					<div class="col-md-4">

						<div data-total="6" data-correct="4" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	La Croix-rouge
			        </div>
						</div>
						<div data-total="6" data-correct="6" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	Emmaüs
			        </div>
						</div>
						<div data-total="6" data-correct="1" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	Les Banques alimentaires
			        </div>
						</div>
						<div data-total="6" data-correct="3" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	Le Téléthon
			        </div>
						</div>
						<div data-total="6" data-correct="2" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	Les Clown de l'espoir
			        </div>
						</div>
						<div data-total="6" data-correct="5" class="draggable panel panel-default element">
							<div class="panel-heading grab">
								<i class="fa fa-arrows"></i>
							</div>
							<div class="panel-body">
			        	Le SamuSocial
			        </div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-3">
				<a class="btn btn-warning reset">Recommencer</a>
			</div>
		</div>
-->
	</div>

  <!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>

  <script>
	var correctAnswers = 0;

	$(document).ready(function() {
		$('.image-link').magnificPopup({
				type:'image',
			});
		$('.video-link').magnificPopup({
				type:'iframe',
				iframe: {
			    patterns: {
			      dailymotion: {

			        index: 'dailymotion.com',

			        id: function(url) {
			            var m = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
			            if (m !== null) {
			                if(m[4] !== undefined) {

			                    return m[4];
			                }
			                return m[2];
			            }
			            return null;
			        },

			        src: 'http://www.dailymotion.com/embed/video/%id%'

			      }
			    }
			  }
			});
	});

  $(function() {

		$('.draggable').each(function(){
			$(this).attr('data-originalLeft', $(this).css('left'));
			$(this).attr('data-origionalTop', $(this).css('top'));
		});

		$(".reset").click(function(){
			correctAnswers = 0;
			$('.droppable').each(function(){
				$(this).droppable( 'enable' );
				$(this).removeClass('correct');
			});
			console.log('Click');
			$('.draggable').each(function(){
				console.log('Draggable element');
				$(this).draggable( 'enable' );
				$(this).draggable( 'option', 'revert', true );
				$(this).css({
					'left': $(this).attr('data-originalLeft'),
					'top': $(this).attr('data-origionalTop')
				});
			});
		});

    $(".draggable").draggable({
			handle: "div.panel-heading",
			stack: '#elementsPile div',
			cursor: 'move',
			revert: true
		});

		$(".droppable").droppable({
      drop: handleElementDrop,
			hoverClass: 'hovered'
    });

		function handleElementDrop( event, ui ) {
		  var slotNumber = $(this).attr( 'data-identifier' );
		  var elementNumber = ui.draggable.attr( 'data-correct' );

		  if ( slotNumber == elementNumber ) {
				$(this).addClass( 'correct' );
				ui.draggable.removeClass( 'list-group-item' );
		    ui.draggable.draggable( 'disable' );
		    $(this).droppable( 'disable' );
		    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
		    ui.draggable.draggable( 'option', 'revert', false );
				correctAnswers++;
		  }else{
				$(this).addClass( 'incorrect' );
				var self = $(this);
				setTimeout(function() {
					self.removeClass( 'incorrect' );
		    }, 1000);
			}

			if( correctAnswers == ui.draggable.attr( 'data-total' ) ){
				alert('Bravo, vous avez terminé !');
				correctAnswers = 0;
			}

			return false;

		}

  });
  </script>

</body>
</html>
