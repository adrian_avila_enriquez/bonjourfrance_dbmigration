<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/introjs.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/redactor.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/flatly/bootstrap.min.css" rel="stylesheet"> -->
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style type="text/css">

	blockquote{
			margin-bottom : 0px !important;
	}

	.cursorMove{
		cursor: move;
	}

	body.dragging, body.dragging * {
	  cursor: move !important;
	}

	.dragged {
	  position: absolute;
	  opacity: 0.5;
	  z-index: 2000;
	}

	ol.example li.placeholder {
	  position: relative;
	  /** More li styles **/
	}
	ol.example li.placeholder:before {
	  position: absolute;
	  /** Define arrowhead **/
	}

	.st-block-control {
    	margin: 0px 2em 0px 0px !important;
	}

	.editable-empty, .editable-empty:hover, .editable-empty:focus {
	    font-style: italic;
	    color: #3091BF;
	    text-decoration: none;
	}
	</style>

</head>
<body>

	@include('admin.partials.menu')

	<div class="container">
		<div class="row">
      <div class="col-md-12">

				<div class="page-header">
					<h1>
						<i class="fa fa-pencil-square-o"></i> Édition de la question <small><i>{{ $question->type->name }}</i></small>
					</h1>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading text-center">
						<h3 class="panel-title">Énoncé</h3>
					</div>
					<div class="panel-body" style="padding:0px;">

						<textarea data-questionid="{{ $question->id }}" name="content" class="questionRedactor" id="questionRedactor-{{ $question->id }}">
		          {!! preg_replace( "/\r|\n/", "", $question->content ) !!}
		        </textarea>

					</div>
				</div>

				@include("admin.questions.types." . $question->type->system_name . ".edit")

				<br />

				<div class="btn-group btn-group-justified" role="group">
					<a href="{{ route('lessonEdit', $lesson->id) }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Retour à la leçon</a>
				</div>

      </div>
    </div>
  </div>

  <!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

	<!-- Redactor -->
	<script type="text/javascript" src="{{ asset('js/redactor.js') }}"></script>

	<!-- Language -->
	<script type="text/javascript" src="{{ asset('js/fr.js') }}"></script>

	<!-- Cloudinary Support -->
	<script type="text/javascript" src="{{ asset('js/jquery.ui.widget.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.iframe-transport.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.fileupload.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.cloudinary.js') }}"></script>

	<script type="text/javascript">

	// Fix token mismatch exception
	$.ajaxSetup({
		data: {
			_token: "{{ csrf_token() }}"
		}
	});

	$.fn.editable.defaults.mode = 'inline';

	function deleteEntry(url){
			if (confirm("Êtes-vous sûr de vouloir supprimer ceci? Cette opération est irréversible!")) {
					$.get(url, function(){
						location.reload();
					});
					return false;
			} else {
					return false;
			}
	}

	$(document).ready(function() {

		$("textarea[id^='questionRedactor']").each(function() {
			var question_id = $(this).attr('data-questionid');
			$(this).redactor({
				lang: 'fr',
				autosaveFields: {
					'question_id': question_id
				},
				autosave: '{{ route("quickSaveQuestionContent") }}',
				autosaveOnChange: true,
				autosaveName: 'questionContent',
				changeCallback: function()
			  {
			    $('#modifiedText').empty();
			    console.log(this.code.get());
			    $('#modifiedText').html( this.code.get() );
			  },
				autosaveCallback: function(json)
				{
					//
				},
				autosaveErrorCallback: function(name, json)
				{
					//
				},
				buttons: ['html']
			});
		});

		$("#title").editable({
				emptytext : 'L\'énoncé de votre question. Ex : De quelle couleur est le loup ?'
		});

	});

	</script>

	<!-- Load question type specific js logic -->
	<script type="text/javascript" src="{{ asset('js/questions/types/admin/' . $question->type->system_name . '.js') }}"></script>

</body>
</html>
