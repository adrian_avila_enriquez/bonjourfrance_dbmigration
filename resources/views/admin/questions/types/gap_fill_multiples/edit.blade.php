<style media="screen">
  .gapFillLine{
    cursor: pointer;
  }
  .highlighted{
    cursor: pointer;
  }
</style>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Les emplacements des mots à retirer</h3>
  </div>
  <div class="panel-body">
    <div id="modifiedText">{!! $question->content !!}</div>
  </div>
</div>

<br />

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Mots à retirer</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered table-striped">
      <thead>
          <tr>
            <th>Le mot à retirer</th>
            <th>Position</th>
            <th>Proposition 1</th>
            <th>Proposition 2</th>
            <th>X</th>
          </tr>
      </thead>
      <tbody>
        @foreach($question->gap_fill_multiples as $key => $line)
          <tr class="gapFillLine">
            <td>
              <a href="#" class="line-{{ $key }}-word" id="word" data-identifier="line-{{ $key }}-wordPosition" data-lineid="line-{{ $key }}-positionContainer" data-value="{{ $line->word }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="Le mot à retirer"></a>
              <br />
              <a href="#" class="spaceLine-{{ $key }}-wordFeedback" id="word_feedback" data-value="{{ $line->word_feedback}}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="Le feedback si l'étudiant répond correctement"></a>
            </td>
            <td id="line-{{ $key }}-positionContainer">
              <a href="#" class="line-{{ $key }}-wordPosition" id="word_position" data-value="{{ $line->word_position }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="Le feedback si l'étudiant répond correctement"></a>
            </td>
            <td>
              <a href="#" class="line-{{ $key }}-proposition1" id="proposition_one" data-value="{{ $line->proposition_one}}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="La proposition n°1"></a>
              <br />
              <a href="#" class="spaceLine-1-proposition1Feedback" id="proposition_one_feedback" data-value="{{ $line->proposition_one_feedback}}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="Le feedback si l'étudiant choisit cette proposition"></a>
            </td>
            <td>
              <a href="#" class="line-1-proposition2" id="proposition_two" data-value="{{ $line->proposition_two}}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="La proposition n°2"></a>
              <br />
              <a href="#" class="spaceLine-1-proposition2Feedback" id="proposition_two_feedback" data-value="{{ $line->proposition_two_feedback}}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $line->id }}" data-title="Le feedback si l'étudiant choisit cette proposition"></a>
            </td>
            <td>
              <a onClick="return deleteEntry('{{ route('lineRemove', $line->id) }}?question_id={{ $question->id }}')" href="#">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un trou</a>
    </div>

  </div>
</div>
