<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <table class="table table-striped">
      <tr>
        <td>Catégorie</td>
        <td>Mots</td>
        <td></td>
      </tr>
      @foreach($question->category_classifications as $choice)
      <tr>
      <td><a href="#" id="content" class="choices-{{ $choice->id }}" data-value="{{ $choice->content }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a></td>
      <td><a href="#" id="words" class="words-{{ $choice->id }}" data-value="{{ $choice->words }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a></td>
      <td>
        <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
          <i class="fa fa-trash"></i>
        </a>
      </td>
    </tr>
      @endforeach
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
