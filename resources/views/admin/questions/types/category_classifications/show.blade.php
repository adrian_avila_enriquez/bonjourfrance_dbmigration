<table class="table table-striped">
  <tr>
    <td>Catégorie</td>
    <td>Mots</td>
  </tr>
  @foreach($question->category_classifications as $choice)
  <tr>
  <td>{{ $choice->content }}</td>
  <td>{{ $choice->words }}</td>
</tr>
  @endforeach
</table>
