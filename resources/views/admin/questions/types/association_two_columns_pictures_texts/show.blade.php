<table class="table">
  <thead>
      <tr>
          <th>Colonne fixe</th>
          <th>Colonne mobile</th>
      </tr>
  </thead>
  <tbody>
  @foreach($question->association_two_columns_pictures_texts as $choice)
    @if($choice->fixed_column == 'left')
      <tr>
          <td>
            <img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_100,w_150/{{ $choice->picture_url }}.jpg" height="100" width="150">
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @elseif($choice->fixed_column == 'right')
    <tr>
        <td>
          {{ $choice->right_column }}
        </td>
        <td>
          <img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_100,w_150/{{ $choice->picture_url }}.jpg" height="100" width="150">
        </td>
    </tr>
    @else
      <tr>
          <td>
            <img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_100,w_150/{{ $choice->picture_url }}.jpg" height="100" width="150">
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @endif
  @endforeach
</tbody>
</table>
