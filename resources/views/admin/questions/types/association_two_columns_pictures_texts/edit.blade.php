<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <table class="table">
      <tr>
        @if( sizeof( $question->association_two_columns_pictures_texts ) > 0 )
          @if($question->association_two_columns_pictures_texts[0]->fixed_column == 'left')
            <td>
              Ajouter votre image
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" checked /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
            </td>
            <td></td>
          @elseif($question->association_two_columns_pictures_texts[0]->fixed_column == 'right')
            <td>
              Ajouter votre image
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" checked /> Colonne droite fixe
            </td>
            <td></td>
          @else
            <td>
              Ajouter votre image
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" checked /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
            </td>
            <td></td>
          @endif
        @else
          <td>
            Ajouter votre image
          </td>
          <td>
              <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" checked /> Colonne gauche fixe
          </td>
          <td>
              <input class="fixedColumns" data-url="{{ URL::route('fixedColumnsPictures', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
          </td>
          <td></td>
        @endif
      </tr>
      @foreach($question->association_two_columns_pictures_texts as $key => $choice)
      <tr>
        <td>
          <input name="file" type="file" />
        </td>
        <td>
          <div class="thumbnail-{{ $key }}">
          @if( $choice->picture_url != '' )
            <img src="http://res.cloudinary.com/bdf/image/upload/c_fill,h_100,w_150/{{ $choice->picture_url }}.jpg" height="100" width="150">
          @else
            <img src="http://res.cloudinary.com/bdf/image/upload/c_pad,h_100,w_150/1434130774_editor_image_picture_photo_mlmvax.jpg" height="100" width="150">
          @endif
          </div>
        </td>
          <td class="hidden">
            <a href="#" id="picture_url-{{ $key }}" class="columns" data-value="{{ $choice->picture_url }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          </td>
          <td>
            <a href="#" id="right_column" class="columns" data-value="{{ $choice->right_column }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          </td>
          <td>
            <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
              <i class="fa fa-trash"></i>
            </a>
          </td>
      </tr>
      @endforeach
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
