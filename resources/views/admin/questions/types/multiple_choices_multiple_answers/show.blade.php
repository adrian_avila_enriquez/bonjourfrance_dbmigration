<ul>
  @foreach($question->multiple_choices_multiple_answers as $choice)
    <li>
      @if( $choice->correct ) <i class="fa fa-check-square-o"></i> @else <i class="fa fa-square-o"></i> @endif {{ $choice->content }}
      @if( $choice->feedback != '' )
        <ul class="list-unstyled">
          <li><i class="fa fa-info-circle"></i> <i>{{ $choice->feedback }}</i></li>
        </ul>
      @endif
    </li>
  @endforeach
</ul>
