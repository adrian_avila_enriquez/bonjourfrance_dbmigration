<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <ol>
      @foreach($question->multiple_choices_multiple_answers as $choice)
      <li id="choice_{{ $choice->id }}">
        <input class="answers" type="checkbox" data-url="{{ URL::route('checkLineCheckbox', [$question->id, $choice->id]) }}" name="answers" @if( $choice->correct) checked @endif> <a href="#" id="content" class="choices-{{ $choice->id }}" data-value="{{ $choice->content }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
        <span class="pull-right">
          <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
            <i class="fa fa-trash"></i>
          </a>
        </span>
        <ul class="list-unstyled">
          <li><i class="fa fa-info-circle"></i> <i><a href="#" id="feedback" class="feedback" data-value="{{ $choice->feedback }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a></i></li>
        </ul>
      </li>
      @endforeach
    </ol>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
