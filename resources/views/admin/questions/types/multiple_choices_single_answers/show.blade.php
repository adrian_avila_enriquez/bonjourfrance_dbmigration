<ul>
  @foreach($question->multiple_choices_single_answers as $choice)
    <li>
      @if( $choice->correct ) <i class="fa fa-check-circle-o"></i> @else <i class="fa fa-circle-o"></i> @endif {{ $choice->content }}
      @if( $choice->feedback != '' )
        <ul class="list-unstyled">
          <li><i class="fa fa-info-circle"></i> <i>{{ $choice->feedback }}</i></li>
        </ul>
      @endif
    </li>
  @endforeach
</ul>
