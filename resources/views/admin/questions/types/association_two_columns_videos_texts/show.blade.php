<table class="table">
  <thead>
      <tr>
          <th>Colonne fixe</th>
          <th>Colonne mobile</th>
      </tr>
  </thead>
  <tbody>
  @foreach($question->association_two_columns_videos_texts as $choice)
    @if($choice->fixed_column == 'left')
      <tr>
          <td>
            <iframe frameborder="0" width="100%" height="150" src="{{ $choice->video_url }}" allowfullscreen></iframe>
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @elseif($choice->fixed_column == 'right')
    <tr>
        <td>
          {{ $choice->right_column }}
        </td>
        <td>
          <iframe frameborder="0" width="100%" height="150" src="{{ $choice->video_url }}" allowfullscreen></iframe>
        </td>
    </tr>
    @else
      <tr>
          <td>
            <iframe frameborder="0" width="100%" height="150" src="{{ $choice->video_url }}" allowfullscreen></iframe>
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @endif
  @endforeach
</tbody>
</table>
