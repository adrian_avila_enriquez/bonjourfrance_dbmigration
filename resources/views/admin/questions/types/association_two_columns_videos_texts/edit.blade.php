<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <table class="table">
      <tr>
          <td>
            Adresse de la vidéo (Youtube ou Dailymotion)
          </td>
          <td>
              Prévisualisation
          </td>
          <td>
              Colonne de droite
          </td>
          <td></td>
      </tr>
      @foreach($question->association_two_columns_videos_texts as $key => $choice)
      <tr>
        <td>
          <a href="#" id="video_url" class="columns" data-value="{{ $choice->video_url }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
        </td>
        <td>
          @if( $choice->video_url != '' )
            {!! generateVideoIframe($choice->video_url) !!}
          @else
            <img src="http://res.cloudinary.com/bdf/image/upload/c_pad,h_100,w_150/1434130774_editor_image_picture_photo_mlmvax.jpg" height="100" width="150">
          @endif
        </td>
          <td>
            <a href="#" id="right_column" class="columns" data-value="{{ $choice->right_column }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          </td>
          <td>
            <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
              <i class="fa fa-trash"></i>
            </a>
          </td>
      </tr>
      @endforeach
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
