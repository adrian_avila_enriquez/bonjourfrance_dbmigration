<div class="alert alert-info" role="alert"><strong>Information</strong> : Le feedback ne sera affiché que en cas de mauvaise réponse de l'apprenant.</div>

<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <table class="table table-stripped">
      <tr>
        <td>Proposition</td>
        <td>Vrai</td>
        <td>Faux</td>
        <td></td>
      </tr>
      @foreach($question->true_falses as $choice)
      <tr>
        <td>
          <a href="#" id="content" class="propositions-{{ $choice->id }}" data-value="{{ $choice->content }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          <br />
          <i><a href="#" id="feedback" class="feedback-{{ $choice->id }}" data-value="{{ $choice->feedback }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a></i>
        </td>
        @if( $choice->answer == '')
          <td><input data-url="{{ route('checkLineRadioTrueFalses', array($question->id, $choice->id, 'true') ) }}" class="answersRadios" name="answer-{{ $choice->id }}" value="true" type="radio" checked></td>
          <td><input data-url="{{ route('checkLineRadioTrueFalses', array($question->id, $choice->id, 'false') ) }}" class="answersRadios" name="answer-{{ $choice->id }}" value="false" type="radio"></td>
        @else
          <td><input data-url="{{ route('checkLineRadioTrueFalses', array($question->id, $choice->id, 'true') ) }}" class="answersRadios" name="answer-{{ $choice->id }}" value="true" type="radio" @if( $choice->answer == 'true') checked @endif></td>
          <td><input data-url="{{ route('checkLineRadioTrueFalses', array($question->id, $choice->id, 'false') ) }}" class="answersRadios" name="answer-{{ $choice->id }}" value="false" type="radio" @if( $choice->answer == 'false') checked @endif></td>
        @endif
        <td>
          <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
            <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
