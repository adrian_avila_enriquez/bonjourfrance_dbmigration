<div class="panel panel-default">
  <div class="panel-heading text-center">
    <h3 class="panel-title">Propositions de réponses</h3>
  </div>
  <div class="panel-body">

    <table class="table">
      <tr>
        @if( sizeof( $question->association_two_columns_texts_texts ) > 0 )
          @if($question->association_two_columns_texts_texts[0]->fixed_column == 'left')
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" checked /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
            </td>
            <td></td>
          @elseif($question->association_two_columns_texts_texts[0]->fixed_column == 'right')
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" checked /> Colonne droite fixe
            </td>
            <td></td>
          @else
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" checked /> Colonne gauche fixe
            </td>
            <td>
                <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
            </td>
            <td></td>
          @endif
        @else
          <td>
              <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'left']) }}" type="radio" id="left_column" name="fixed_column" value="left" /> Colonne gauche fixe
          </td>
          <td>
              <input class="fixedColumns" data-url="{{ URL::route('fixedColumns', [$question->id, 'right']) }}" type="radio" id="right_column" name="fixed_column" value="right" /> Colonne droite fixe
          </td>
          <td></td>
        @endif
      </tr>
      @foreach($question->association_two_columns_texts_texts as $choice)
      <tr>
          <td>
            <a href="#" id="left_column" class="columns" data-value="{{ $choice->left_column }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          </td>
          <td>
            <a href="#" id="right_column" class="columns" data-value="{{ $choice->right_column }}" data-type="text" data-url="{{ route('quickPostLine', $question->id) }}" data-pk="{{ $choice->id }}" data-title=""></a>
          </td>
          <td>
            <a onClick="return deleteEntry('{{ route('lineRemove', $choice->id) }}?question_id={{ $question->id }}')" href="#">
              <i class="fa fa-trash"></i>
            </a>
          </td>
      </tr>
      @endforeach
    </table>

    <br />

    <div class="text-center" role="group">
      <a href="{{ route('addLine', $question->id) }}?question_type_id={{ $question->type->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une proposition</a>
    </div>

  </div>
</div>
