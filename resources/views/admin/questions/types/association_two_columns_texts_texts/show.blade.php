<table class="table">
  <thead>
      <tr>
          <th>Colonne fixe</th>
          <th>Colonne mobile</th>
      </tr>
  </thead>
  <tbody>
  @foreach($question->association_two_columns_texts_texts as $choice)
    @if($choice->fixed_column == 'left')
      <tr>
          <td>
            {{ $choice->left_column }}
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @elseif($choice->fixed_column == 'right')
      <tr>
          <td>
            {{ $choice->right_column }}
          </td>
          <td>
            {{ $choice->left_column }}
          </td>
      </tr>
    @else
      <tr>
          <td>
            {{ $choice->left_column }}
          </td>
          <td>
            {{ $choice->right_column }}
          </td>
      </tr>
    @endif
  @endforeach
</tbody>
</table>
