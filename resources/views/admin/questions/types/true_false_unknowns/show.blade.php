<ul>
  @foreach($question->true_false_unknowns as $choice)
    <li>{{ $choice->content }}<br /><i class="fa fa-info-circle"></i> <i>{{ $choice->feedback }}</i></li>
  @endforeach
</ul>
