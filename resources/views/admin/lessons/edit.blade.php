<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/sir-trevor.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/introjs.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/redactor.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/flatly/bootstrap.min.css" rel="stylesheet"> -->
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style type="text/css">
	blockquote{
			margin-bottom : 0px !important;
	}

	.cursorMove{
		cursor: move;
	}

	body.dragging, body.dragging * {
	  cursor: move !important;
	}

	.dragged {
	  position: absolute;
	  opacity: 0.5;
	  z-index: 2000;
	}

	ol.example li.placeholder {
	  position: relative;
	  /** More li styles **/
	}
	ol.example li.placeholder:before {
	  position: absolute;
	  /** Define arrowhead **/
	}

	.st-block-control {
    	margin: 0px 2em 0px 0px !important;
	}

	.editable-empty, .editable-empty:hover, .editable-empty:focus {
	    font-style: italic;
	    color: #3091BF;
	    text-decoration: none;
	}
	</style>

</head>
<body>

	@include('admin.partials.menu')

	<div class="container">
		<div class="content">

			<div class="col-md-12">
				<form>

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="lesson_id" name="lesson_id" value="{{ $lesson->id }}">

					<div class="page-header">
						<h1 data-intro="Bienvenue dans l'interface pour créer une leçon ! Nous allons voir pas-à-pas comment créer une leçon ! C'est parti !">
							<i class="fa fa-pencil-square-o"></i> Édition de la leçon
							<span class="pull-right">
								@if( $lesson->active )
									<a href="{{ route('toggleLessonState', $lesson->id) }}?active=0"><i class="fa fa-toggle-on"></i></a>
								@else
									<a href="{{ route('toggleLessonState', $lesson->id) }}?active=1"><i class="fa fa-toggle-off"></i></a>
								@endif
								 /
								<a onClick="return deleteEntry()" href="{{ route('lessonRemove', $lesson->id) }}"><i class="fa fa-trash"></i></a>
								/
								<a href="{{ route('lesson', $lesson->id) }}">
									<i class="fa fa-search"></i>
								</a>
							</span>
						</h1>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3 class="panel-title" data-intro="Tout d'abord, il faut ajouter un titre à la leçon.">Titre</h3>
						</div>
						<div class="panel-body text-center">
							<h2 href="#" id="title" data-type="text" data-pk="{{ $lesson->id }}">{{ $lesson->title }}</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">

							<div class="panel status panel-default">
								<div class="panel-heading text-center">
									<h2 class="panel-title" data-intro="Ensuite, vous devez indiquer le niveau de votre leçon.">Niveau</h2>
								</div>
								<div class="panel-body text-center">
									<h3><a href="#" id="level_id" data-value="{{ $lesson->level_id }}" data-source="{{ $levels }}" data-type="select" data-pk="{{ $lesson->id }}" data-title=""></a></h3>
								</div>
							</div>

						</div>
						<div class="col-md-4">
							<div class="panel status panel-default">
								<div class="panel-heading text-center">
									<h2 class="panel-title" data-intro="Vous devez indiquer la rubrique dans laquelle vous voulez insérer votre leçon.">Rubrique</h2>
								</div>
								<div class="panel-body text-center">
									<h3><a href="#" id="category_id" data-value="{{ $lesson->category_id }}" data-source="{{ $categories }}" data-type="select" data-pk="{{ $lesson->id }}"></a></h3>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel status panel-default">
								<div class="panel-heading text-center">
									<h2 class="panel-title" data-intro="Ensuite, il faut indiquer à quel numéro de Bonjour de France appartient votre leçon.">Numéro</h2>
								</div>
								<div class="panel-body text-center">
									<h3><a href="#" id="number_id" data-value="{{ $lesson->number_id }}" data-type="select" data-pk="{{ $lesson->id }}" data-title=""></a></h3>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3 class="panel-title">Description</h3>
						</div>
						<div class="panel-body">

						<blockquote data-intro="Il faut ensuite donner une description de votre leçon.">
							<p><a href="#" id="description" data-type="textarea" data-pk="{{ $lesson->id }}">{{ $lesson->description }}</a></p>
						</blockquote>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<h3 class="panel-title">La leçon</h3>
						</div>
						<div class="panel-body" style="padding:0px;">

							<textarea data-test="testingValue" name="lesson" id="lessonRedactor">
								{!! preg_replace( "/\r|\n/", "", $lesson->content ) !!}
							</textarea>

						</div>
						<div class="panel-footer" style="color:#817F7F;"><span id="saveCallbackContainer">En attente de modifications...</span></div>
					</div>

					<div class="page-header" id="exercicesAnchor">
						<h1><i class="fa fa-pencil-square-o"></i> Édition des exercices <small class="pull-right">

							<a onClick="return addExercice('{{ route("addExercice", $lesson->id) }}')" id="addExerciceButton" class="btn btn-info" href="#"><i class="fa fa-plus"></i> Ajouter un exercice</a>

							</small></h1>
					</div>

					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<strong>Information : </strong> Si vous faites des changements d'ordre des éléments, veuillez recharger la page pour voir apparaître les modifications.
					</div>

					<div id="exercicesContainer">
						@include('admin.partials.exercices')
					</div>

					<div class="btn-group btn-group-justified" role="group">
						<a href="{{ route('lessons') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Retour à la liste des leçons</a>
					</div>

				</form>
			</div>

		</div>
	</div>

	<div class="container-fluid text-center">
	  <hr>
	    <div class="row">
	        <div class="col-lg-12">
	            <ul class="nav nav-pills nav-justified">
	                <li><a href="/">© {{ date('Y') }} Bonjour de France.</a></li>
	                <li><a href="#">Conditions d'utilisation</a></li>
	                <li><a href="#">Vie privée</a></li>
	            </ul>
	        </div>
	    </div>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

	<script type="text/javascript" src="{{ asset('js/intro.min.js') }}"></script>

	<!-- Redactor -->
		<script type="text/javascript" src="{{ asset('js/redactor.js') }}"></script>

	<!-- Language -->
		<script type="text/javascript" src="{{ asset('js/fr.js') }}"></script>

	<!-- Plugins -->
		<script type="text/javascript" src="{{ asset('js/video.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/imagemanager.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/table.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/fontcolor.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/filemanager.js') }}"></script>

	<script type="text/javascript" src="{{ asset('js/jquery.mjs.nestedSortable.js') }}"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

	<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	{!! Toastr::render() !!}

	<script>

	function addExercice(url){
			$.get(url, function(){
				location.reload();
			});
			return false;
	}

		function deleteEntry(url){
				if (confirm("Êtes-vous sûr de vouloir supprimer ceci? Cette opération est irréversible!")) {
						$.get(url, function(){
							location.reload();
						});
						return false;
				} else {
						return false;
				}
		}

		// Fix token mismatch exception
		$.ajaxSetup({
			data: {
				_token: "{{ csrf_token() }}"
			}
		});

		// Xeditable
		$.fn.editable.defaults.url = '{{ route("quickPostLesson") }}';
		$.fn.editable.defaults.mode = 'inline';
		$(document).ready(function() {

			if (location.hash){
				// @TODO
			}

			var addQuestionUrl = "{{ route('addQuestion', $lesson->id) }}";

			$('select[id^="addQuestion"]').on('change', function(){
				console.log( $(this) );

				var exercice_id = $(this).find(":selected").attr('data-exerciceid');
				var question_type_id = $(this).find(":selected").val();

				if( $('.addQuestion :selected').val() != 0 ){
					window.location.href = addQuestionUrl + '?exercice_id=' + exercice_id + '&question_type_id=' + question_type_id;
				}
			});

			/*
			$('#addExerciceButton').on('click', function(e){
				e.preventDefault();
				$.get('{{ route("addExercice", $lesson->id) }}', function(data){
					$("#sortable").append(data).promise().done(function(){
						$("a[class^='exerciceTitle']").each(function(index) {
							$(this).editable({
								emptytext : 'Complément du titre de votre exercice. Par défaut "Exercice n°' + ( parseInt(index) + parseInt(1) ) + '".'
							})
						});
						$("a[class^='questionTitle']").each(function(index) {
							$(this).editable({
								emptytext : 'L\'énoncé de votre question. Ex : De quelle couleur est le loup ?'
							})
						});
						$("#sortable").sortable('refresh');
					});

				});

			});
			*/

			$(function() {
				$( "#sortable" ).nestedSortable({
					handle: ".handle",
					startCollapsed: true,
					protectRoot : true,
					items: 'li',
					maxLevels: 2,
					isTree: true,
					update: function (event, ui) {

						var data = $("#sortable").nestedSortable('toHierarchy', {startDepthCount: 0});

						$.post("{{ route('sortElements') }}", {sort : data}, function(result){
							console.log(result);
						});

						var $lis = $(this).children('li');
		        $lis.each(function() {
		            var $li = $(this);
		            var newVal = $(this).index() + 1;
		            $(this).children('.sortable-number').html(newVal);
		            $(this).children('#item_display_order').val(newVal);
		        });

					}
				});
			});

			function updateLessonSaving(){
				$('#saveCallbackContainer').text('Enregistrement en cours...');
				setTimeout(function(){
					$('#saveCallbackContainer').text('Enregistré');
				}, 2000);
			}

			function updateExerciceSaving(exercice_id){
				$('#saveExerciceCallbackContainer-' + exercice_id).text('Enregistrement en cours...');
				setTimeout(function(){
					$('#saveExerciceCallbackContainer-' + exercice_id).text('Enregistré');
				}, 2000);
			}

			function errorOnSaving(){
				$('#saveCallbackContainer').text('Erreur lors de l\'enregistrement!');
			}

			function errorOnExerciceSaving(exercice_id){
				$('#saveExerciceCallbackContainer-' + exercice_id).text('Erreur lors de l\'enregistrement!');
			}

			// Redactor
			$('#lessonRedactor').redactor({
				lang: 'fr',
				imageUpload : "{{ URL::to('admin/postImage') }}",
				autosaveFields: {
					'lesson_id': '#lesson_id',
					'testing_field' : $('#lessonRedactor').attr('data-test')
				},
				autosave: '{{ route("quickSaveLessonContent") }}',
				autosaveOnChange: true,
				autosaveName: 'lessonContent',
				autosaveCallback: function(json)
				{
					updateLessonSaving();
				},
				autosaveErrorCallback: function(name, json)
				{
					errorOnSaving();
				},
				plugins: ['video', 'imagemanager', 'table', 'fontcolor', 'filemanager'],
				imageManagerJson: '{{ route("imageGallery") }}',
				fileUpload: "{{ URL::to('admin/postImage') }}",
				fileManagerJson: '{{ route("fileGallery") }}',
			});

			$('#helpLink').on('click', function(){
				introJs().start();
			});

			// Questions
			/*
			$("a[class^='questionTitle']").each(function(index) {
				$(this).editable({
					emptytext : 'L\'énoncé de votre question. Ex : De quelle couleur est le loup ?'
				})
			});
			*/

			// Exercices
			$("a[class^='exerciceTitle']").each(function(index) {
				$(this).editable({
					emptytext : 'Complément du titre de votre exercice (facultatif)'
				})
			});

			$("textarea[id^='exerciceRedactor']").each(function() {
				var exercice_id = $(this).attr('data-exerciceid');
				$(this).redactor({
					lang: 'fr',
					imageUpload : "{{ URL::to('admin/postImage') }}",
					autosaveFields: {
						'exercice_id': exercice_id
					},
					autosave: '{{ route("quickSaveExerciceContent") }}',
					autosaveOnChange: true,
					autosaveName: 'exerciceContent',
					autosaveCallback: function(json)
					{
						updateExerciceSaving( exercice_id );
					},
					autosaveErrorCallback: function(name, json)
					{
						errorOnExerciceSaving( exercice_id );
					},
					plugins: ['video', 'imagemanager', 'table', 'fontcolor', 'filemanager'],
					imageManagerJson: '{{ route("imageGallery") }}',
					fileUpload: "{{ URL::to('admin/postImage') }}",
					fileManagerJson: '{{ route("fileGallery") }}',
				});
			});

			$('#title').editable({
				emptytext : 'Titre non-renseigné (Brouillon)'
			});

			$('#description').editable({
				inputclass: 'myTextarea',
				emptytext : 'Donnez envie aux étudiants de suivre votre leçon (Environ 200 caractères)'
			});

			$('#level_id').editable({
				showbuttons: false,
				emptytext : 'Ex : A1 - Débutant, A2 - Élémentaire, etc...'
		   });

		   $('#category_id').editable({
		    	showbuttons: false,
		    	emptytext : 'Ex : Grammaire, Compréhension, etc...'
		   });

		   $('#number_id').editable({
		    	showbuttons: false,
		    	emptytext : 'Le numéro de Bonjour de France',
		        source: [
		              {value: 32, text: '32 (numéro actuel)'},
		              {value: 33, text: '33'},
		              {value: 34, text: '34'}
		           ]
		    });
		});

	</script>

</body>
</html>
