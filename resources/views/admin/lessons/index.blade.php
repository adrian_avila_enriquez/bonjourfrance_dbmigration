<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/flatly/bootstrap.min.css" rel="stylesheet"> -->
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	@include('admin.partials.menu')

	<div class="container">
		<div class="content">

			<div class="col-md-12">

				<div class="page-header">
					<h1 data-intro="Bienvenue dans l'interface pour consulter vos leçons ! C'est parti !"><i class="fa fa-pencil-square-o"></i> Liste de mes leçons <small class="pull-right"><a id="helpLink" class="btn btn-info">Besoin d'aide ?</a></small></h1>
				</div>

				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>Notre conseil : </strong> Avant de rédiger votre leçon, posez-vous les bonnes questions : <br /><br /><ul><li>De quoi va parler ma leçon ?</li><li>Quel est le public de ma leçon ?</li><li>Quels sont les objectifs de ma leçon ?</li><li>Quel est le plan de ma leçon ?</li></ul><br /> Si vous avez besoin d'aide, n'hésitez pas à nous envoyer un mail avec vos questions et plans, nous nous ferons un plaisir de les étudier et de vous aidez dans la construction de votre leçon.
				</div>

				@if ( $lessons->count() )
				<table id="lessons" class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Titre</th>
							<th>Niveau</th>
							<th>Numéro</th>
							<th>Rubrique</th>
							<th>Nombre d'exercices</th>
							<th>Status</th>
							<th>Supprimer</th>
						</tr>
					</thead>
					<tbody>
						@foreach($lessons as $lesson)
							<tr>
								<td>
									<a href="{{ route('lessonEdit', $lesson->id) }}">
										@if( $lesson->title != '' )
											{{ $lesson->title }}
										@else
											Brouillon n°{{ $lesson->id }}
										@endif
									</a>
								</td>
								<td>@if( $lesson->level_id != 0 ) {{ $lesson->level->name }} @else Non précisé @endif</td>
								<td>@if( $lesson->number_id != 0 ) {{ $lesson->number_id }} @else Non précisé @endif</td>
								<td>@if( $lesson->category_id != 0 ) {{ $lesson->category->name }} @else Non précisé @endif</td>
								<td>{{ $lesson->exercices->count() }}</td>
								<td>@if( $lesson->active ) Activée @else Désactivée @endif</td>
							  <td class="text-center"><a onClick="return deleteEntry()" href="{{ route('lessonRemove', $lesson->id) }}"><i class="fa fa-trash"></i></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
				@else
					<div class="alert alert-info" role="alert">Pas encore de leçon. Pour en créer une <a href="{{ route('lessonEdit') }}">cliquez ici</a></div>
				@endif
			</div>

		</div>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

	<script>

	$(document).ready(function() {
		$('#lessons').DataTable({
			language: {
	        processing:     "Traitement en cours...",
	        search:         "Rechercher&nbsp;:",
	        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
	        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
	        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
	        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
	        infoPostFix:    "",
	        loadingRecords: "Chargement en cours...",
	        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
	        emptyTable:     "Aucune donnée disponible dans le tableau",
	        paginate: {
	            first:      "Premier",
	            previous:   "Pr&eacute;c&eacute;dent",
	            next:       "Suivant",
	            last:       "Dernier"
	        },
	        aria: {
	            sortAscending:  ": activer pour trier la colonne par ordre croissant",
	            sortDescending: ": activer pour trier la colonne par ordre décroissant"
	        }
	    }
		});
	});

	function deleteEntry(){
	    if (confirm("Êtes-vous sûr de vouloir supprimer cette leçon? Cette opération est irréversible!")) {
	        return true;
	    } else {
	        return false;
	    }
	}
	</script>

</body>
</html>
