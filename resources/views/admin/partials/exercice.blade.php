<li class="list-group-item" id="exercice_{{ $exercice->id }}">
  <div>

    <span class="page-header">
      <h3>
        <i class="fa fa-arrows handle cursorMove"></i>
        <span style="font-weight:bold;">Exercice n°{{ $exercice->order }} : </span>
        <a href="#" id="title" class="exerciceTitle-{{ $exercice->id }}" data-value="{{ $exercice->title }}" data-type="text" data-url="{{ route('quickPostExercice') }}" data-pk="{{ $exercice->id }}" data-title=""></a>
        <span class="pull-right">
          <a onClick="return deleteEntry('{{ route('exerciceRemove', $exercice->id) }}?lesson_id={{ $lesson->id }}')" href="#">
            <i class="fa fa-trash"></i>
          </a>
        </span>
      </h3>
    </span>

    <br />

    <div class="panel panel-default">
      <div class="panel-heading text-center">
        <h3 class="panel-title">Contenu additionnel (facultatif)</h3>
      </div>
      <div class="panel-body" style="padding:0px;">

        <textarea data-exerciceid="{{ $exercice->id }}" name="content" id="exerciceRedactor-{{ $exercice->id }}">
          {!! preg_replace( "/\r|\n/", "", $exercice->content ) !!}
        </textarea>

      </div>
      <div class="panel-footer" style="color:#817F7F;"><span id="saveExerciceCallbackContainer-{{ $exercice->id }}">En attente de modifications...</span></div>
    </div>

  </div>

  <span class="page-header">
    <h4 class="handle"><b>Questions</b> <small class="pull-right">
      @if( $exercice->questions->count() > 0 )
        <a class="btn btn-warning btn-sm" href="{{ route('addQuestion', $lesson->id) }}?exercice_id={{ $exercice->id }}&question_type_id={{ $exercice->questions[0]->type->id }}"><i class="fa fa-plus"></i> Ajouter une question</a>
      @else
        <select class="form-control" id="addQuestion-{{ $exercice->id }}">
          <option value="0">>> Choisir un type de question</option>
          @foreach($question_types as $question_type_category)
            <optgroup label="{{ $question_type_category->name }}">
              @foreach($question_type_category->question_types as $question_type)
                <option data-exerciceid="{{ $exercice->id }}" value="{{ $question_type->id }}">{{ $question_type->name }}</option>
              @endforeach
            </optgroup>
          @endforeach
        </select>
      @endif
    </small></h4>
  </span>

  <br />

  @if( $exercice->questions->count() > 0 )
    <ol class="list-group">

      @foreach($exercice->questions as $question)

        @include('admin.partials.question')

      @endforeach

    </ol>
  @else
    <div class="alert alert-info" role="alert">Pas encore de question. Pour en créer une, choisissez un type de question dans le menu déroulant <span style="margin-right:15px;" class="pull-right"><i class="fa fa-hand-o-up fa-lg"></i></span></div>
  @endif
</li>
