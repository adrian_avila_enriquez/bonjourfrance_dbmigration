<li class="list-group-item" id="question_{{ $question->id }}">
  <div>
    <span style="font-weight:bold;"><i class="fa fa-arrows handle cursorMove"></i><span class="pull-right">
      <a onClick="return deleteEntry('{{ route('questionRemove', $question->id) }}?lesson_id={{ $lesson->id }}')" href="#">
        <i class="fa fa-trash"></i>
      </a>
    </span></span>
    <span style="font-weight:bold;" class="sortable-number">{{ $exercice->order }} - {{ $question->order }}</span>
    <i>{{ $question->type->name }}</i> <a href="{{ route('questionEdit', $question->id) }}?lesson_id={{ $lesson->id }}">(Éditer)</a>
    <br /><br / >
    <blockquote>{!! $question->content !!}</blockquote>

    <br />

    @include('admin.questions.types.' . $question->type->system_name . '.show')

  </div>
</li>
