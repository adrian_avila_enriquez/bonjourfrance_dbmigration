@if( $lesson->exercices->count() > 0 )
  <ol id="sortable" class="list-group">

    @foreach($lesson->exercices as $exercice)

      @include('admin.partials.exercice')

    @endforeach

  </ol>
@else
  <div class="alert alert-info" role="alert">Pas encore d'exerices. Pour en créer un <a href="{{ route('addExercice', $lesson->id) }}">cliquez ici</a></div>
@endif
