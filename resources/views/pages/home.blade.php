home.blade.php

<hr />

{{ App::getLocale() }}

<hr />

@foreach($routes as $route)
  @if( $route->display_home_page )
    <a href="{{ URL::to('/') }}/{{ $route->public_url }}">Lien</a>
  @endif
@endforeach
