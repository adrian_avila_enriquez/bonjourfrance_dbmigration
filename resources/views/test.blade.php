<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/sir-trevor.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/introjs.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/redactor.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/flatly/bootstrap.min.css" rel="stylesheet"> -->
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style type="text/css">
	blockquote{
			margin-bottom : 0px !important;
	}

	.cursorMove{
		cursor: move;
	}

	body.dragging, body.dragging * {
	  cursor: move !important;
	}

	.dragged {
	  position: absolute;
	  opacity: 0.5;
	  z-index: 2000;
	}

	ol.example li.placeholder {
	  position: relative;
	  /** More li styles **/
	}
	ol.example li.placeholder:before {
	  position: absolute;
	  /** Define arrowhead **/
	}

	.st-block-control {
    	margin: 0px 2em 0px 0px !important;
	}

	.editable-empty, .editable-empty:hover, .editable-empty:focus {
	    font-style: italic;
	    color: #3091BF;
	    text-decoration: none;
	}
	</style>

</head>
<body>

  <div class="container">
    <div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Le texte</h3>
				</div>
				<div class="panel-body">
      		<textarea style="height:100%;" class="form-control" id="originalText">{{ $question->content }}</textarea>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Les emplacements des mots à retirer</h3>
				</div>
				<div class="panel-body">
      		<p id="modifiedText">{{ $question->content }}</p>
				</div>
			</div>

			<br />

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Mots à retirer</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered table-striped">
		        <thead>
		            <tr>
									<th>Le mot à retirer</th>
		              <th>Position</th>
		              <th>Proposition 1</th>
		              <th>Proposition 2</th>
		            </tr>
		        </thead>
		        <tbody>
							@foreach($question->gap_fill_multiples_lines as $key => $line)
			          <tr class="gapFillLine">
			            <td>
			              <a href="#" class="line-{{ $key }}-word" id="word-{{ $key }}" data-lineid="line-{{ $key }}-positionContainer" data-value="{{ $line->word }}" data-type="text" data-url="test/" data-pk="{{ $line->id }}" data-title="Le mot à retirer"></a>
			              <br />
			              <a href="#" class="line-{{ $key }}-wordFeedback" id="wordFeedback-{{ $key }}" data-value="{{ $line->word_feedback}}" data-type="text" data-url="test/" data-pk="1" data-title="Le feedback si l'étudiant répond correctement"></a>
			            </td>
			            <td id="line-{{ $key }}-positionContainer">{{ $line->word_position + 1 }}</td>
			            <td>
			              <a href="#" class="line-{{ $key }}-proposition1" id="proposition1-{{ $key }}" data-value="{{ $line->proposition_one}}" data-type="text" data-url="test/" data-pk="1" data-title="La proposition n°1"></a>
			              <br />
			              <a href="#" class="line-1-proposition1Feedback" id="proposition1Feedback-{{ $key }}" data-value="{{ $line->proposition_one_feedback}}" data-type="text" data-url="test/" data-pk="1" data-title="Le feedback si l'étudiant choisit cette proposition"></a>
			            </td>
			            <td>
			              <a href="#" class="line-1-proposition2" id="proposition2-{{ $key }}" data-value="{{ $line->proposition_two}}" data-type="text" data-url="test/" data-pk="1" data-title="La proposition n°2"></a>
			              <br />
			              <a href="#" class="line-1-proposition2Feedback" id="proposition2Feedback-{{ $key }}" data-value="{{ $line->proposition_two_feedback}}" data-type="text" data-url="test/" data-pk="1" data-title="Le feedback si l'étudiant choisit cette proposition"></a>
			            </td>
			          </tr>
							@endforeach
						</tbody>
		      </table>
				</div>
			</div>

    </div>
  </div>

  <!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

	<!-- Redactor -->
	<script type="text/javascript" src="{{ asset('js/redactor.js') }}"></script>

	<!-- Language -->
	<script type="text/javascript" src="{{ asset('js/fr.js') }}"></script>

  <style media="screen">
		.gapFillLine{
			cursor: pointer;
		}
	  .highlighted{
	    background-color: orange;
			cursor: pointer;
			padding: 2px 3px 2px 3px;
	  }
	  .highlighted.selected{
	    background-color: blue;
			color: #fff;
	  }
  </style>

  <script>

	$('#originalText').redactor({
		buttons: ['html'],
		changeCallback: function()
		{
			$('#modifiedText').empty();
			console.log(this.code.get());
			$('#modifiedText').html( this.code.get() );
		}
	});

	function resetTrClass(){
		$('.gapFillLine').each(function(index){
			$(this).removeClass('info');
		});
	}

  // Xeditable
  $.fn.editable.defaults.mode = 'inline';
  $("a[class^='line']").each(function(index) {
    $(this).editable();
  });

  $('.gapFillLine').on('click', function(){
		resetTrClass();
		$(this).addClass('info');
    var firstAnchor = $(this).find( "td > a" )[ 0 ];
    var wordToFind = $( firstAnchor ).attr('data-value');
    var lineId = $( firstAnchor ).attr('data-lineid');

		var currentWordIndex = $('#' + lineId).text();
		var realCurrentWordIndex = parseInt(currentWordIndex) - parseInt(1);

    var text = $('#modifiedText').text();
    var re = new RegExp(wordToFind + '[(,| |;|.|?|:)]', 'gi');

    var replaceIndex = 0;
		var word_found = false;

    var new_text = text.replace(re, function(i, text){
			if( realCurrentWordIndex == replaceIndex){
				word_found = true;
				var temp = " <span data-position='" + replaceIndex + "' data-lineid='" + lineId + "' class='highlighted selected'>" + wordToFind + "</span> ";
			}else{
				var temp = " <span data-position='" + replaceIndex + "' data-lineid='" + lineId + "' class='highlighted'>" + wordToFind + "</span> ";
			}
      replaceIndex++;
      return temp;
    });

		if( ! word_found){
			alert('Attention, le mot n\'a pas était trouvé dans le texte à la position indiquée.');
		}

		$('#modifiedText').html(new_text);

  });

	function changePosition(element, position){
		var userPosition = parseInt(position) + parseInt(1);
		$('#' + element).html( userPosition );
	}

  $(document).on("click", "span.highlighted", function () {
		$('span.highlighted').each(function(index){
			$(this).removeClass('selected');
		});
		$(this).addClass('selected');
    var lineId = $(this).attr('data-lineid');
    var position = $(this).attr('data-position');
		changePosition(lineId, position);
  });

  </script>

</body>
</html>
