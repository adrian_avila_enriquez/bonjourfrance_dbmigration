<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.4/united/bootstrap.min.css" rel="stylesheet">
	<link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.3.0/animate.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  <style>
		.grab{cursor:move;}
		.draggable{overflow:hidden;z-index:99;}
    .droppable { background-color: grey; }
		.hovered{ background-color: #eee; }
		.correct{ background-color: green; }
		.incorrect{ background-color: red; }
		.element{min-height:150px;max-height:150px;}
		.image-link{cursor:zoom-in;}
		* {
		  border-radius: 0 !important;
		}
  </style>

</head>
<body>

  <div class="container">

	  <div class="row">
	    <div class="col-md-9">
	      <h1>{{ $lesson->title }}</h1>
	      <h2>{{ $lesson->description }}</h2>

	      {!! $lesson->content !!}

	      @foreach($lesson->exercices as $key => $exercice)

	        <h3><strong>Exercice n°{{ $key + 1 }}</strong><i>{{ ' : ' . $exercice->title }}</i></h3>
	        <p>{!! $exercice->content !!}</p>
	        <hr />

	        @foreach($exercice->questions as $subKey => $question)

						<h4><strong>Question n°{{ $subKey + 1 }}</strong><i>{{ ' : ' . $question->title }}</i></h4>

						@if(
							$question->type->system_name != 'gap_fill_multiples'
							&& $question->type->system_name != 'gap_fill_singles'
						)
	          	<p>{!! $question->content !!}</p>
						@endif

						<div class="alert alert-info" role="alert"><strong>Instructions techniques</strong> : {{ $question->type->instructions }}</div>

	          @include('public.questions.types.' . $question->type->system_name)

	        @endforeach

	      @endforeach
	    </div>
			<div class="col-md-3">
			</div>
	  </div>

	</div>

	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />

	<!-- Scripts -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>

	<script>

		var correct_answer = 'Bravo ! C\'est une bonne réponse !';

			(function($){

			$.fn.shuffle = function() {

					var allElems = this.get(),
							getRandom = function(max) {
									return Math.floor(Math.random() * max);
							},
							shuffled = $.map(allElems, function(){
									var random = getRandom(allElems.length),
											randEl = $(allElems[random]).clone(true)[0];
									allElems.splice(random, 1);
									return randEl;
						});

					this.each(function(i){
							$(this).replaceWith($(shuffled[i]));
					});

					return $(shuffled);

			};

		})(jQuery);
	</script>

	<!-- Load question type specific js logic -->
	<script type="text/javascript" src="{{ asset('js/questions/types/public/association_two_columns_pictures_texts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/association_two_columns_videos_texts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/multiple_choices_single_answers.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/multiple_choices_multiple_answers.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/gap_fill_multiples.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/gap_fill_singles.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/true_falses.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/true_false_unknowns.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/questions/types/public/category_classifications.js') }}"></script>

</body>
</html>
