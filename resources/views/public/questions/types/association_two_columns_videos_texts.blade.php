<div class="draggableContainer">
  @foreach($question->association_two_columns_videos_texts->shuffle() as $choice)
    <div class="row">
      <div class="col-md-8 col-xs-6">

        <div class="row">
          <div class="col-md-6 col-xs-6 element">
            {!! generateVideoIframe($choice->video_url) !!}
          </div>
          <div data-questionid="{{ $question->id}}" data-identifier="{{ $choice->id }}" class="droppable droppable-{{ $question->id }} col-md-6 col-xs-6 element"></div>
        </div>

      </div>
      <div class="col-md-4 col-xs-6">

        <div data-total="{{ $question->association_two_columns_videos_texts->count() }}" data-correct="{{ $choice->id }}" class="draggable draggable-{{ $question->id }} panel panel-default element">
          <div class="panel-heading grab">
            <i class="fa fa-arrows"></i>
          </div>
          <div class="panel-body">
            {{ $choice->right_column }}
          </div>
        </div>

      </div>
    </div>
  @endforeach
</div>

<br />

<div id="feedback-association_two_columns_videos_texts-{{ $question->id }}"></div>
