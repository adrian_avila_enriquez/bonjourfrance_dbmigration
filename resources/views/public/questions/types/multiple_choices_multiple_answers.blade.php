@foreach($question->multiple_choices_multiple_answers as $choice)
  <div class="checkbox">
    <label>
      <input data-choice="{{ $choice->content }}" class="correct-multiple_choices_multiple_answers-{{ $question->id }}" type="checkbox" data-correct='{{ $choice->correct }}' data-feedback="{{ $choice->feedback }}" value="">
      {{ $choice->content }}
    </label>
  </div>
@endforeach

<div id="feedback-multiple_choices_multiple_answers-{{ $question->id }}"></div>

<a class="btn btn-success" data-id="{{ $question->id }}" id="correct-multiple_choices_multiple_answers-{{ $question->id }}">Corriger la question</a>
