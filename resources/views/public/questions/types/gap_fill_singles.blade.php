<p style="line-height:2.5em;">{!! generateGapFillSingle($question->id, $question->content, $question->gap_fill_singles) !!}</p>

<br />
<br />

<div id="feedback-gap_fill_singles-{{ $question->id }}"></div>

<a class="btn btn-success" data-id="{{ $question->id }}" id="correct-gap_fill_singles-{{ $question->id }}">Corriger la question</a>
