{!! generateGapFillMultiple($question->id, $question->content, $question->gap_fill_multiples) !!}

<br />
<br />

<div id="feedback-gap_fill_multiples-{{ $question->id }}"></div>

<a class="btn btn-success" data-id="{{ $question->id }}" id="correct-gap_fill_multiples-{{ $question->id }}">Corriger la question</a>
