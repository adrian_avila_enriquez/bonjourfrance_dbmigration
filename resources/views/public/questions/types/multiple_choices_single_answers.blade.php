@foreach($question->multiple_choices_single_answers as $choice)
  <div class="radio">
    <label>
      <input data-choice="{{ $choice->content }}" class="correct-multiple_choices_single_answers-{{ $question->id }}" type="radio" data-correct='{{ $choice->correct }}' name="optionsRadios-{{ $question->id }}" id="optionsRadios1" value="option1" data-feedback="{{ $choice->feedback }}">
      {{ $choice->content }}
    </label>
  </div>
@endforeach

<div id="feedback-multiple_choices_single_answers-{{ $question->id }}"></div>

<a class="btn btn-success" data-id="{{ $question->id }}" id="correct-multiple_choices_single_answers-{{ $question->id }}">Corriger la question</a>
