<table class="table table-stripped">
  <tr>
      <td>Proposition</td>
      <td>Vrai</td>
      <td>Faux</td>
  </tr>
  @foreach($question->true_falses as $choice)
  <tr>
    <td>{{ $choice->content }}</td>
    <td><input data-choice="{{ $choice->content }}" class="correct-true_falses-{{ $question->id }}" type="radio" @if( $choice->answer == 'true' ) data-correct='1' @else data-correct='0' @endif name="optionsRadios-{{ $question->id }}-{{ $choice->id }}" id="optionsRadios1" value="option1" data-feedback="{{ $choice->feedback }}"></td>
    <td><input data-choice="{{ $choice->content }}" class="correct-true_falses-{{ $question->id }}" type="radio" @if( $choice->answer == 'false' ) data-correct='1' @else data-correct='0' @endif name="optionsRadios-{{ $question->id }}-{{ $choice->id }}" id="optionsRadios1" value="option1" data-feedback="{{ $choice->feedback }}"></td>
  </tr>
@endforeach
</table>

<div id="feedback-true_falses-{{ $question->id }}"></div>

<a class="btn btn-success" data-id="{{ $question->id }}" id="correct-true_falses-{{ $question->id }}">Corriger la question</a>
