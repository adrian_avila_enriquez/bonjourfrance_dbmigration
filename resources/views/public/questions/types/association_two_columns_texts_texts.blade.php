<div class="draggableContainer">
  @foreach($question->association_two_columns_texts_texts as $choice)
    @if($question->association_two_columns_texts_texts[0]->fixed_column == 'left')
      <div class="row">
        <div class="col-md-8">

          <div class="row">
            <div class="col-md-6">
              <div class="panel element panel-default">
                <div class="panel-body">
                  <p>{{ $choice->left_column }}</p>
                </div>
              </div>
            </div>
            <div data-questionid="{{ $question->id}}" data-identifier="{{ $choice->id }}" class="droppable droppable-{{ $question->id }} col-md-6 element"></div>
          </div>

        </div>
        <div class="col-md-4">

          <div data-total="{{ $question->association_two_columns_texts_texts->count() }}" data-correct="{{ $choice->id }}" class="draggable draggable-{{ $question->id }} panel panel-default element">
            <div class="panel-heading grab">
              <i class="fa fa-arrows"></i>
            </div>
            <div class="panel-body">
              {{ $choice->right_column }}
            </div>
          </div>

        </div>
      </div>
    @elseif($question->association_two_columns_texts_texts[0]->fixed_column == 'right')
      <div class="row">
        <div class="col-md-8">

          <div class="row">
            <div class="col-md-6">
              <div class="panel element panel-default">
                <div class="panel-body">
                  <p>{{ $choice->right_column }}</p>
                </div>
              </div>
            </div>
            <div data-questionid="{{ $question->id}}" data-identifier="{{ $choice->id }}" class="droppable droppable-{{ $question->id }} col-md-6 element"></div>
          </div>

        </div>
        <div class="col-md-4">

          <div data-total="{{ $question->association_two_columns_texts_texts->count() }}" data-correct="{{ $choice->id }}" class="draggable draggable-{{ $question->id }} panel panel-default element">
            <div class="panel-heading grab">
              <i class="fa fa-arrows"></i>
            </div>
            <div class="panel-body">
              {{ $choice->left_column }}
            </div>
          </div>

        </div>
      </div>
    @else
      <div class="row">
        <div class="col-md-8">

          <div class="row">
            <div class="col-md-6">
              <div class="panel element panel-default">
                <div class="panel-body">
                  <p>{{ $choice->left_column }}</p>
                </div>
              </div>
            </div>
            <div data-questionid="{{ $question->id}}" data-identifier="{{ $choice->id }}" class="droppable droppable-{{ $question->id }} col-md-6 element"></div>
          </div>

        </div>
        <div class="col-md-4">

          <div data-total="{{ $question->association_two_columns_texts_texts->count() }}" data-correct="{{ $choice->id }}" class="draggable draggable-{{ $question->id }} panel panel-default element">
            <div class="panel-heading grab">
              <i class="fa fa-arrows"></i>
            </div>
            <div class="panel-body">
              {{ $choice->right_column }}
            </div>
          </div>

        </div>
      </div>
    @endif
  @endforeach
</div>

<br />

<div id="feedback-association_two_columns_texts_texts-{{ $question->id }}"></div>
