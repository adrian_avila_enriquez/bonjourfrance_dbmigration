<div class="categoryClassificationContainer-{{ $question->id }}">
  <u><h4>Propositions</h4></u>
  <div class="row">
    <div class="col-md-12">
    @foreach($question->category_classifications as $choice)
      <a data-categoryid="{{ $choice->id }}" class="category-category_classification-{{ $question->id }} btn btn-lg btn-primary">{{ $choice->content }}</a>
    @endforeach
    </div>
  </div>

  <br />

  <u><h4>Éléments de réponse</h4></u>
  <div class="row">
    <div class="categoryClassificationWordsContainer-{{ $question->id }} col-md-12">
    @foreach($question->category_classifications->shuffle() as $choice)
      {!! generateCategoryClassification($question->id, $choice->id, $choice->words) !!}
    @endforeach
    </div>
  </div>
</div>

<div class="hidden" id="feedback-category_classifications-{{ $question->id }}">
  <div class="alert alert-success" role="alert"><strong>Bravo !</strong> Voici le résumé des réponses.</div>
  @foreach($question->category_classifications as $choice)
    <a class="btn btn-lg btn-primary">{{ $choice->content }}</a>
    <br /><br />
    <ul>
      <li>
        {!! generateCategoryClassification($question->id, $choice->id, $choice->words, $choice->id) !!}
      </li>
    </ul>
  @endforeach
</div>
