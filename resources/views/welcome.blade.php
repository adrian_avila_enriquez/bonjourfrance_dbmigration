<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/sir-trevor.css') }}" rel="stylesheet" type="text/css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style type="text/css">
	.st-block-control {
	    margin: 0px 2em 0px 0px !important;
	}
	.editable-empty, .editable-empty:hover, .editable-empty:focus {
	    font-style: italic;
	    color: #3091BF;
	    text-decoration: none;
	}
	</style>

</head>
<body>

	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Flexo-Machine</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="#">Accueil</a></li>
	        <li class="active"><a href="#">Créer une leçon <span class="sr-only">(current)</span></a></li>
	        <li><a href="#">Mes activités</a></li>
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="#">Aide</a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ludovic Floch <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="#">Mon profil</a></li>
	            <li class="divider"></li>
	            <li><a href="#">Déconnexion</a></li>
	          </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="container">
		<div class="content">

			<div class="col-md-12">
				<form method="POST" action='{{ URL::to('saveCourse') }}'>

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="course_id" value="{{ $course->id }}">

					<div class="row">
						<div class="col-md-12 text-center">
							<h1>
								<i style="font-size:20px;" class="fa fa-quote-left"></i> <span class="font-size: 36px;color:#343434;" href="#" id="title" data-type="text" data-pk="{{ $course->id }}" data-url="{{ URL::to('quickPostCourse') }}">{{ $course->title }}</span> <i style="font-size:20px;" class="fa fa-quote-right"></i>
							</h1>
						</div>
					</div>
					
					<hr />
					
					<h2>1. Niveau : <a style="font-size: 20px;" href="#" id="level_id" data-value="{{ $course->level_id }}" data-type="select" data-pk="{{ $course->id }}" data-url="{{ URL::to('quickPostCourse') }}" data-title=""></a></h2>

					<h2>2. Rubrique : <a style="font-size: 20px;" href="#" id="category_id" data-value="{{ $course->category_id }}" data-type="select" data-pk="{{ $course->id }}" data-url="{{ URL::to('quickPostCourse') }}"></a></h2>

					<h2>3. Description du cours : <span style="font-size: 20px;" href="#" id="description" data-type="textarea" data-pk="{{ $course->id }}" data-url="{{ URL::to('quickPostCourse') }}">{{ $course->description }}</span></h2>
					
					<hr />

					<h2>4. La leçon :</h2>

					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<strong>Notre conseil : </strong> Avant de rédiger votre leçon, posez-vous les bonnes questions : <br /><br /><ul><li>De quoi va parler mon cours ?</li><li>Quel est le public de mon cours ?</li><li>Quels sont les objectifs de mon cours ?</li><li>Quel est le plan de mon cours ?</li></ul><br /> Si vous avez besoin d'aide, n'hésitez pas à nous envoyer un mail avec vos questions et plans, nous nous ferons un plaisir de les étudier et de vous aidez dans la construction de votre cours.
					</div>

					<textarea name="lesson" class="js-st-instance">{{ $course->lesson }}</textarea>

					<hr />

					<button class="btn btn-success btn-lg btn-block" type="submit">Enregistrer</button>

				</form>
			</div>
			
		</div>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/eventable/1.0.4/eventable.js"></script>
	<script type="text/javascript" src="{{ asset('js/sir-trevor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/iframe.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/fr.js') }}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

	<script>

		// Fix token mismatch exception
		$.ajaxSetup({
			data: {
				_token: "{{ csrf_token() }}"
			}
		});

		// Xeditable
		$.fn.editable.defaults.mode = 'inline';
		$(document).ready(function() {
			$('#title').editable({
				emptytext : 'Saisir le titre de votre cours ici'
			});
			$('#description').editable({
				emptytext : 'Saisir la description votre cours'
			});
			$('#level_id').editable({   
				emptytext : 'Le niveau du cours. Ex : Débutant, Élémentaire, etc...',
		        source: [
		              {value: 1, text: 'Débutant'},
		              {value: 2, text: 'Élémentaire'},
		              {value: 3, text: 'Intermédiaire'},
		              {value: 4, text: 'Avancé'}
		           ]
		    });
		    $('#category_id').editable({   
		    	emptytext : 'La rubrique du cours. Ex : Grammaire, Compréhension, etc...',
		        source: [
		              {value: 1, text: 'Grammaire'},
		              {value: 2, text: 'Compréhension'},
		              {value: 3, text: 'Vocabulaire'},
		              {value: 4, text: 'Histoire de la francophonie'}
		           ]
		    });
		});

		// SirTrevor
		SirTrevor.config.language = "fr";

		new SirTrevor.Editor({ 
			el: $('.js-st-instance'),
			blockTypes: ["Heading", "Text", "Quote", "List", "Image", "Video", "Iframe"]
		});

	</script>

</body>
</html>