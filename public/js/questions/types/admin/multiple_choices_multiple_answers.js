$(".feedback").editable({
    emptytext : 'Feedback pour l\'apprenant.'
});

// Choices
$("a[class^='choices']").each(function(index) {
  $(this).editable({
    emptytext : 'Le contenu de votre proposition'
  })
});

$('.answers').on('click', function(){
  var url = $(this).attr('data-url');

  var state = false;

  if( $(this).prop('checked') ){
    var state = true;
  }

  $.get(url + '/' + state, function(){
    console.log('ok');
  });
});
