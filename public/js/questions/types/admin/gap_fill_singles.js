function resetTrClass(){
  $('.gapFillLine').each(function(index){
    $(this).removeClass('info');
  });
}

// Xeditable
$.fn.editable.defaults.mode = 'inline';
$("a[class^='spaceLine']").each(function(index) {
  $(this).editable();
});
$("a[class^='line']").each(function(index) {
  $(this).editable({
    validate: function(value) {
        if(/\s/.test(value)) {
            return 'Les espaces sont interdits.';
        }
    }
  });
});

$('.gapFillLine').on('click', function(){
  console.log('click');
  resetTrClass();
  $(this).addClass('info');
  var firstAnchor = $(this).find( "td > a" )[ 0 ];
  var wordToFind = $( firstAnchor ).editable('getValue');
  console.log(wordToFind);
  wordToFind = wordToFind.word;
  console.log(wordToFind);
  var lineId = $( firstAnchor ).attr('data-lineid');

  var currentWordIndex = $('#' + lineId).text();
  var realCurrentWordIndex = currentWordIndex;

  var text = $('.questionRedactor').redactor('code.get');
  console.log(text);
  var re = new RegExp('[(,|;| |.|?|:|!)]' + wordToFind + '[(,|;| |.|?|:|!)]', 'gi');

  console.log(re);

  var replaceIndex = 1;
  var word_found = false;

  var new_text = text.replace(re, function(i, text){
    if( realCurrentWordIndex == replaceIndex){
      word_found = true;
      var temp = " <span data-position='" + replaceIndex + "' data-lineid='" + lineId + "' class='highlighted label label-success'>" + wordToFind + "</span> ";
    }else{
      var temp = " <span data-position='" + replaceIndex + "' data-lineid='" + lineId + "' class='highlighted label label-default'>" + wordToFind + "</span> ";
    }
    replaceIndex++;
    return temp;
  });

  if( ! word_found && wordToFind != '' ){
    alert('Attention, le mot n\'a pas était trouvé dans le texte à la position indiquée.');
  }

  $('#modifiedText').html(new_text);

});

function changePosition(element, position){
  console.log('element : ' + element);
  var temp = element.split('-');
  var userPosition = position;
  $('.line-' + temp[1] + '-wordPosition').editable( 'setValue', userPosition );
  var url = $('.line-' + temp[1] + '-wordPosition').attr('data-url');
  var pk = $('.line-' + temp[1] + '-wordPosition').attr('data-pk');
  $('.line-' + temp[1] + '-wordPosition').editable( 'submit', {url:url} );
  $.get(url, {pk: pk, name:'word_position',value:position}, function(){
    console.log('Send');
  });
}

$(document).on("click", "span.highlighted", function () {
  $('span.highlighted').each(function(index){
    $(this).removeClass('label-success');
    $(this).addClass('label-default');
  });
  $(this).addClass('label-success');
  var lineId = $(this).attr('data-lineid');
  var position = $(this).attr('data-position');
  changePosition(lineId, position);
});
