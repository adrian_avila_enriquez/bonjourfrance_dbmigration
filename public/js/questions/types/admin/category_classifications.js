// Choices
$("a[class^='choices']").each(function(index) {
  $(this).editable({
    emptytext : 'La catégorie'
  })
});

$("a[class^='words']").each(function(index) {
  $(this).editable({
    emptytext : 'Les mots pour cette catégorie'
  })
});
