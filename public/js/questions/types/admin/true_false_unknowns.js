$(".feedback").editable({
    emptytext : 'Feedback pour l\'apprenant.'
});

// Choices
$("a[class^='propositions']").each(function(index) {
  $(this).editable({
    emptytext : 'Le contenu de votre proposition'
  })
});

$("a[class^='feedback']").each(function(index) {
  $(this).editable({
    emptytext : 'Le feedback en cas de mauvaise réponse'
  })
});

$('.answersRadios').on('click', function(){
  var url = $(this).attr('data-url');
  $.get(url, function(){
    console.log('ok');
  });
});

$('input[class=answersRadios]:checked').each(function(){
  var url = $(this).attr('data-url');
  $.get(url, function(){
    console.log('ok');
  });
});
