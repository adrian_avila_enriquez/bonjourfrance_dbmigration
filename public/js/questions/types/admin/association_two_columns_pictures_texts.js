// Choices
$("a[class^='columns']").each(function(index) {
  $(this).editable({
    emptytext : 'Le contenu la colonne'
  })
});

$('.fixedColumns').on('click', function(){

  var url = $(this).attr('data-url');

  $.get(url, function(){
    console.log('ok');
  });
});

$.get( $('input[name=fixedColumns]:checked').attr('data-url'), function(){
  console.log('ok');
});

$.cloudinary.config({ cloud_name: 'bdf', api_key: '492812336224234'})

$('input[type=file]').each(function(index){

  $(this).unsigned_cloudinary_upload("fplqx1td",
    { cloud_name: 'bdf', tags: 'browser_uploads' },
    { multiple: false }
  ).bind('cloudinarydone', function(e, data) {

    $('#picture_url-' + index).editable( 'setValue', data.result.public_id, true );

    var url = $('#picture_url-' + index).attr('data-url');
    var pk = $('#picture_url-' + index).attr('data-pk');

    $.get(url, {pk: pk, name:'picture_url',value:data.result.public_id}, function(){
      console.log('Send');
    });

    $('.thumbnail-' + index).html($.cloudinary.image(data.result.public_id,
      { format: 'jpg', width: 150, height: 100,
        crop: 'pad' } ))}

  ).bind('cloudinaryprogress', function(e, data) {
    console.log('progress');
    $('.progress_bar').css('width',
      Math.round((data.loaded * 100.0) / data.total) + '%');

  });
});
