$(".feedback").editable({
    emptytext : 'Feedback pour l\'apprenant.'
});

// Choices
$("a[class^='choices']").each(function(index) {
  $(this).editable({
    emptytext : 'Le contenu de votre proposition'
  })
});

$('.answersRadios').on('click', function(){
  var url = $(this).attr('data-url');
  $.get(url, function(){
    console.log('ok');
  });
});
