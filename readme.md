Flexomachine Data Base migration
-------------------------------------------
**Description**

This repository is a replicate of an old version of the repo : https://...@bitbucket.org/TeddyBear06/bonjour-de-france-laravel-5.git. It contains a script for migrating the data from an old verison of the Flexomachine to its new DB schema. The only significative difference is in the file routes.php  

**Requirements** :

1. Set a DB Connection to the **old** Flexomachine DB. The configuration can be found in /config/database.php named as **mysql** . Be sure to be using the latest DB dump when doing this.

2. Set a DB Connection to the **new** Flexomachine DB. The configuration can be found in /config/database.php named as **mysql2** .

----------

**Execution**

 a) For executing the script with the USERS migration, go to: http://{your path...}/database_migration/public/index.php/migrate_users

 a) For executing the script with the LESSONS migration, go to: http://{your path...}/database_migration/public/index.php/database


**Attention !!**

Dont try to migrate and seed from this repository. If you do, it will create inconsistencies in the DB. If you want to migrate and seed, do it from the original repositoy : TeddyBear06/bonjour-de-france-laravel-5


**Recommended usage**

1) Create a fresh new DB from the original repository.
2) Migrate and seed from this same directory.
3) Run the USERS migration script.
4) Run the LESSONS migration script.

