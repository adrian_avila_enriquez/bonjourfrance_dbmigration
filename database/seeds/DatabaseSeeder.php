<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use \App\Models\Category;
use \App\Models\QuestionTypeCategory;
use \App\Models\Level;
use \App\Models\Exercice;
use \App\Models\QuestionType;
use \App\Models\Question;
use \App\Models\Lesson;
use \App\Models\User;
use \App\Models\AppRoute;

// Question types
use \App\Models\QuestionTypes\MultipleChoicesSingleAnswers;
use \App\Models\QuestionTypes\MultipleChoicesMultipleAnswers;
use \App\Models\QuestionTypes\GapFillMultiples;
use \App\Models\QuestionTypes\GapFillSingles;
use \App\Models\QuestionTypes\AssociationTwoColumnsTextsTexts;
use \App\Models\QuestionTypes\AssociationTwoColumnsPicturesTexts;
use \App\Models\QuestionTypes\AssociationTwoColumnsVideosTexts;
use \App\Models\QuestionTypes\TrueFalseUnknowns;
use \App\Models\QuestionTypes\TrueFalses;
use \App\Models\QuestionTypes\CategoryClassifications;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CategoryTableSeeder');
		$this->call('QuestionTypeCategoryTableSeeder');
		$this->call('LessonTableSeeder');
		$this->call('QuestionTypeTableSeeder');
		$this->call('LevelTableSeeder');
		$this->call('ExerciceTableSeeder');
		$this->call('QuestionTableSeeder');
		$this->call('MultipleChoiceSingleAnswerTableSeeder');
		$this->call('MultipleChoiceMultipleAnswerTableSeeder');
		$this->call('GapFillMultiplesTableSeeder');
		$this->call('GapFillSinglesTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('AppRouteTableSeeder');
		$this->call('AssociationTwoColumnsPicturesTextsTableSeeder');
		$this->call('AssociationTwoColumnsTextsTextsTableSeeder');
		$this->call('AssociationTwoColumnsVideosTextsTableSeeder');
		$this->call('TrueFalseUnknownsTableSeeder');
		$this->call('TrueFalsesTableSeeder');
		$this->call('CategoryClassificationsTableSeeder');
	}

}

class QuestionTypeCategoryTableSeeder extends Seeder {

    public function run()
    {
        DB::table('question_type_categories')->delete();
				QuestionTypeCategory::create(array(
					'name' => 'QCM',
					'created_at' => new DateTime,
					'updated_at' => new DateTime
				));
				QuestionTypeCategory::create(array(
					'name' => 'Associations',
					'created_at' => new DateTime,
					'updated_at' => new DateTime
				));
				QuestionTypeCategory::create(array(
					'name' => 'Texte lacunaire',
					'created_at' => new DateTime,
					'updated_at' => new DateTime
				));
				QuestionTypeCategory::create(array(
					'name' => 'Vrai ou faux',
					'created_at' => new DateTime,
					'updated_at' => new DateTime
				));
				QuestionTypeCategory::create(array(
					'name' => 'Classement',
					'created_at' => new DateTime,
					'updated_at' => new DateTime
				));
			}
		}

class CategoryClassificationsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('category_classifications')->delete();
				CategoryClassifications::create(array(
                'question_id' => 8,
                'content' => 'Les fruits',
                'feedback' => '',
                'words' => 'Les fraises, Les abricots, Les figues',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				CategoryClassifications::create(array(
                'question_id' => 8,
                'content' => 'Les légumes',
                'feedback' => '',
                'words' => 'Les pommes de terre, Les poireaux, Les radis',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				CategoryClassifications::create(array(
                'question_id' => 8,
                'content' => 'Les arbres',
                'feedback' => '',
                'words' => 'Les pruniers, Les sapins, Les acacias',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class TrueFalsesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('true_falses')->delete();
				TrueFalses::create(array(
                'question_id' => 9,
                'content' => 'Le cheval blanc d\'Henry 4 est blanc vrai ou faux ?',
                'feedback' => 'Comment avez-vous pu faire une erreur... Allez encore un effort.',
                'answer' => 'true',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				TrueFalses::create(array(
                'question_id' => 9,
                'content' => 'Le cheval blanc d\'Henry 8 est rouge ?',
                'feedback' => 'Aïe, il est rouge ce cheval et non blanc!',
                'answer' => 'false',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				TrueFalses::create(array(
                'question_id' => 9,
                'content' => '1 + 1 = 2 ?',
                'feedback' => 'Comment avez-vous pu faire une erreur... Allez encore un effort.',
                'answer' => 'true',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class TrueFalseUnknownsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('true_false_unknowns')->delete();
				TrueFalseUnknowns::create(array(
                'question_id' => 5,
                'content' => 'Le cheval blanc d\'Henry 4 est blanc ?',
                'feedback' => 'Comment avez-vous pu faire une erreur... Allez encore un effort.',
                'answer' => 'true',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				TrueFalseUnknowns::create(array(
                'question_id' => 5,
                'content' => 'Le cheval blanc d\'Henry 8 est rouge ?',
                'feedback' => 'Aïe, il est rouge ce cheval et non blanc!',
                'answer' => 'false',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				TrueFalseUnknowns::create(array(
                'question_id' => 5,
                'content' => 'Le cheval de couleur inconnue d\'Henry 8 est bleu ?',
                'feedback' => 'Il n\'est pas possible de savoir de quelle couleur il est...',
                'answer' => 'unknown',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class AssociationTwoColumnsTextsTextsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('association_two_columns_texts_texts')->delete();
				AssociationTwoColumnsTextsTexts::create(array(
                'question_id' => 10,
                'fixed_column' => 'left',
                'left_column' => 'Près de Monaco',
                'right_column' => 'Menton : La ville aux agrumes.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				AssociationTwoColumnsTextsTexts::create(array(
                'question_id' => 10,
                'fixed_column' => 'left',
                'left_column' => 'Près de Nice',
                'right_column' => 'Villefranche : Les couleurs du Sud.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				AssociationTwoColumnsTextsTexts::create(array(
                'question_id' => 10,
                'fixed_column' => 'left',
                'left_column' => 'Près de Cannes',
                'right_column' => 'Antibes : Le soleil, la mer....',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class AssociationTwoColumnsVideosTextsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('association_two_columns_videos_texts')->delete();
				AssociationTwoColumnsVideosTexts::create(array(
                'question_id' => 12,
                'fixed_column' => 'left',
                'video_url' => 'http://www.dailymotion.com/video/x2u3rwf_final-fantasy-vii-remake-ps4-trailer-e3-2015_videogames',
                'right_column' => 'Un clip d\'un jeu vidéo.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				AssociationTwoColumnsVideosTexts::create(array(
                'question_id' => 12,
                'fixed_column' => 'left',
                'video_url' => 'https://www.youtube.com/watch?v=Cvjtc4Vh-fo',
                'right_column' => 'Un clip musical.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				AssociationTwoColumnsVideosTexts::create(array(
                'question_id' => 12,
                'fixed_column' => 'left',
                'video_url' => 'http://www.dailymotion.com/video/x2ow9ha_compilation-the-best-of-red-bull-rampage-2014-i-downhill-crash-gopro_sport',
                'right_column' => 'Un reportage sportif.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class AssociationTwoColumnsPicturesTextsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('association_two_columns_pictures_texts')->delete();
				AssociationTwoColumnsPicturesTexts::create(array(
                'question_id' => 4,
                'fixed_column' => 'left',
                'picture_url' => 'antibes_yiszil',
                'right_column' => 'Menton : La ville aux agrumes.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				AssociationTwoColumnsPicturesTexts::create(array(
                'question_id' => 4,
                'fixed_column' => 'left',
                'picture_url' => 'villefranche_tokvqc',
                'right_column' => 'Villefranche : Les couleurs du Sud.',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class AppRouteTableSeeder extends Seeder {

    public function run()
    {
        DB::table('app_routes')->delete();
				// Home
					AppRoute::create(array(
						'type' => 'home',
						'lang' => 'en-GB',
						'public_url' => 'learn-french-online',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'home',
						'lang' => 'fr-FR',
						'public_url' => '/',
						'active' => 1
	        ));
				// Choose level
					AppRoute::create(array(
						'type' => 'choose_level',
						'lang' => 'en-GB',
						'category_id' => 1,
						'display_home_page' => 1,
						'public_url' => 'learn-french-online/grammar/choose-level',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'choose_level',
						'lang' => 'fr-FR',
						'category_id' => 1,
						'display_home_page' => 1,
						'public_url' => 'index/indexgram.htm',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'choose_level',
						'lang' => 'en-GB',
						'category_id' => 3,
						'display_home_page' => 0,
						'public_url' => 'learn-french-online/vocabulary/choose-level',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'choose_level',
						'lang' => 'fr-FR',
						'category_id' => 3,
						'display_home_page' => 0,
						'public_url' => 'index/indexvoca.htm',
						'active' => 1
	        ));
				// Choose lesson
					AppRoute::create(array(
						'type' => 'choose_lesson',
						'lang' => 'en-GB',
						'category_id' => 1,
						'level_id' => 1,
						'public_url' => 'learn-french-online/grammar/exercices-beginner',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'choose_lesson',
						'lang' => 'fr-FR',
						'category_id' => 1,
						'level_id' => 1,
						'public_url' => 'grammaire-francaise/exercice-debutant',
						'active' => 1
	        ));
				// Lesson
	        AppRoute::create(array(
						'type' => 'lesson',
						'lang' => 'en-GB',
						'lesson_id' => 1,
						'public_url' => 'comprehension/discover-something.html',
						'active' => 1
	        ));
					AppRoute::create(array(
						'type' => 'lesson',
						'lang' => 'fr-FR',
						'lesson_id' => 1,
						'public_url' => 'contenu/exercices/entrez-dans-mon-univers.html',
						'active' => 1
	        ));
    }
}

class GapFillSinglesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('gap_fill_singles')->delete();
				GapFillSingles::create(array(
                'question_id' => 11,
                'word' => 'boule',
                'word_feedback' => 'Un indice... C\'est rond !',
                'word_position' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				GapFillSingles::create(array(
                'question_id' => 11,
                'word' => 'rouge',
                'word_feedback' => 'C\'est la même couleur que la pomme gala...',
                'word_position' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class GapFillMultiplesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('gap_fill_multiples')->delete();
				GapFillMultiples::create(array(
                'question_id' => 3,
                'word' => 'de',
                'word_feedback' => '',
                'word_position' => 1,
                'proposition_one' => 'Manger',
                'proposition_one_feedback' => 'Ce n\'est pas manger car...',
                'proposition_two' => 'Boire',
                'proposition_two_feedback' => 'Ce n\'est pas boire parce que...',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				GapFillMultiples::create(array(
                'question_id' => 3,
                'word' => 'promotion',
                'word_feedback' => 'Bravo ! La promotion permet de faire connaître quelque chose !',
                'word_position' => 1,
                'proposition_one' => 'recevoir',
                'proposition_one_feedback' => 'Ce n\'est pas recevoir car...',
                'proposition_two' => 'découvrir',
                'proposition_two_feedback' => 'Ce n\'est pas découvrir parce que...',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class MultipleChoiceMultipleAnswerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('multiple_choices_multiple_answers')->delete();
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 2,
                'content' => 'Proposition 1',
                'feedback' => 'Ce n\'est pas la comédie romantique et science-fiction parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 2,
                'content' => 'Proposition 2',
								'feedback' => 'Bravo ! Il s\'agit bien de la proposition 2 !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 2,
                'content' => 'Proposition 3',
								'feedback' => 'En effet c\'est également une bonne réponse !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 7,
                'content' => 'Mr Bean',
                'feedback' => 'Ce n\'est pas correct parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 7,
                'content' => 'Ken Block',
								'feedback' => 'Bravo ! Il s\'agit bien d\'un vrai champion !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesMultipleAnswers::create(array(
                'question_id' => 7,
                'content' => 'Aaron Gwin',
								'feedback' => 'Oh que oui ! Gagnez "chainless" c\'est la classe ultime !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class MultipleChoiceSingleAnswerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('multiple_choices_single_answers')->delete();
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 1,
                'content' => 'Comédie romantique et science-fiction',
                'feedback' => 'Ce n\'est pas la comédie romantique et science-fiction parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 1,
                'content' => 'Comédie dramatique et fantastique',
								'feedback' => 'Bravo ! Vous avez bien répondu !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 1,
                'content' => 'Drame et film biographique',
								'feedback' => 'Ce n\'est pas un drame et film biographique parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 6,
                'content' => 'Yeti 24"',
                'feedback' => 'Ce n\'est pas la comédie romantique et science-fiction parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 6,
                'content' => 'Spe 26"',
								'feedback' => 'Bravo ! Vous avez bien répondu !',
                'correct' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				MultipleChoicesSingleAnswers::create(array(
                'question_id' => 6,
                'content' => 'Monty 24"',
								'feedback' => 'Ce n\'est pas un Monty parce que...',
                'correct' => 0,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        /*
        DB::table('users')->delete();
        User::create(array(
                'email' => 'ludovic.floch@gmx.com',
                'name' => 'Ludovic Floch',
                'password' => Hash::make('123456'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
        */
}

class QuestionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('questions')->delete();
        Question::create(array(
                'question_type_id' => 1,
                'exercice_id' => 1,
                'content' => 'À quels genres appartient ce film ?',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 2,
                'exercice_id' => 2,
                'content' => 'Où se déroule l’histoire ?',
                'order' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 4,
                'exercice_id' => 3,
                'content' => 'Bonjour de France est un cyber-magazine éducatif gratuit contenant des exercices, des tests et des jeux pour apprendre le français ainsi que des fiches pédagogiques à l’attention des enseignants de français langue étrangère (FLE). À travers ses différentes pages, ce magazine se veut aussi un outil de promotion de la francophonie. De nouvelles rubriques interactives apparaîtront régulièrement afin que ce site soit pleinement un lieu de ressource et de partage.',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 5,
                'exercice_id' => 4,
                'content' => 'Amusez-vous en déplaçant des éléments !',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 6,
                'exercice_id' => 5,
                'content' => 'À vous de jouer !',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 1,
                'exercice_id' => 1,
                'content' => 'Lequel est un 26" ?',
                'order' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 2,
                'exercice_id' => 2,
                'content' => 'Lequels sont des champions ?',
                'order' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 7,
                'exercice_id' => 6,
                'content' => 'Un peu de culture',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 8,
                'exercice_id' => 7,
                'content' => 'Vrai ou faux... Humm... Je sais pas !',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 3,
                'exercice_id' => 8,
                'content' => 'Un peu de géographie',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 9,
                'exercice_id' => 9,
                'content' => 'J\'aime le billard. Surtout de bien diriger ma boule dans les angles du plateau de jeu. La plus diffcile pour moi, c\'est la rouge car elle me fait mal aux yeux.',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Question::create(array(
                'question_type_id' => 10,
                'exercice_id' => 10,
                'content' => 'À vous de jouer !',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
		}
}

class ExerciceTableSeeder extends Seeder {

    public function run()
    {
        DB::table('exercices')->delete();
        Exercice::create(array(
                'lesson_id' => 1,
                'title' => 'Compréhension globale',
                'content' => '',
                'order' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => '',
                'content' => '',
                'order' => 2,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => '',
                'content' => '',
                'order' => 3,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => '',
                'content' => '',
                'order' => 4,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Pour aller plus loin...',
                'content' => '<p>Envie de savoir plus sur l’auteur et sur le livre ? Survolez avec votre souris l’image et visitez les liens.</p><iframe src="//www.thinglink.com/card/641999962829225984" type="text/html" scrolling="no" frameborder="0" height="735" width="512"></iframe><p><b>En classe : </b></p><p>A votre avis, pourquoi l’auteur a choisi ce titre "L’écume de jours" ?</p><p>Puis, imaginez et écrivez la fin de cette histoire.</p>',
                'order' => 5,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Le bonus',
                'content' => '',
								'order' => 6,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Le mega bonus',
                'content' => '',
								'order' => 7,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Et c\'est pas fini !',
                'content' => '',
								'order' => 8,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Toujours pas !',
                'content' => '',
								'order' => 9,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Exercice::create(array(
								'lesson_id' => 1,
                'title' => 'Encore pas fini !',
                'content' => '',
								'order' => 10,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
    }
}

class QuestionTypeTableSeeder extends Seeder {

    public function run()
    {
        DB::table('question_types')->delete();
        QuestionType::create(array(
	        'system_name' => 'multiple_choices_single_answers',
					'question_type_category_id' => 1,
	        'name' => 'QCM à réponse unique',
	        'instructions' => 'Cochez la bonne réponse.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'multiple_choices_multiple_answers',
					'question_type_category_id' => 1,
	        'name' => 'QCM à réponses multiples',
	        'instructions' => 'Cochez les bonnes réponses.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'association_two_columns_texts_texts',
					'question_type_category_id' => 2,
	        'name' => 'Association deux colonnes (Texte/Texte)',
	        'instructions' => 'Associez les deux colonnes en faisant glisser les éléments à droite sur la colonne du milieu.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'gap_fill_multiples',
					'question_type_category_id' => 3,
	        'name' => 'Texte lacunaire réponses multiples',
	        'instructions' => 'Pour chacuns des menus déroulants, selectionnez la réponse correcte.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'association_two_columns_pictures_texts',
					'question_type_category_id' => 2,
	        'name' => 'Association deux colonnes (Image/Texte)',
	        'instructions' => 'Associez les deux colonnes en faisant glisser les éléments à droite sur la colonne du milieu.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'true_false_unknowns',
					'question_type_category_id' => 4,
	        'name' => 'Vrai / Faux / On ne sait pas',
	        'instructions' => 'Cochez la bonne réponse. Si vous ne pouvez pas trouver un élément vous indiquant si la réponse est correct ou non, choissisez "On ne sait pas".',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'category_classifications',
					'question_type_category_id' => 5,
	        'name' => 'Classement par catégories',
	        'instructions' => 'Associez les catégories avec l\'ensemble de mots qui lui appartient. Cliquez en premier sur la catégorie, puis cliquez sur les éléments de réponse.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'true_falses',
					'question_type_category_id' => 4,
	        'name' => 'Vrai / Faux',
	        'instructions' => 'Cochez la bonne réponse.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'gap_fill_singles',
					'question_type_category_id' => 3,
	        'name' => 'Texte lacunaire réponses libre',
	        'instructions' => 'Dans chacuns des champs vides, écrivez la bonne réponse.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
				QuestionType::create(array(
					'system_name' => 'association_two_columns_videos_texts',
					'question_type_category_id' => 2,
	        'name' => 'Association deux colonnes (Vidéos/Texte)',
	        'instructions' => 'Associez les deux colonnes en faisant glisser les éléments à droite sur la colonne du milieu.',
	        'state' => 'warning',
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
    }
}

class LessonTableSeeder extends Seeder {

    public function run()
    {
        DB::table('lessons')->delete();
        Lesson::create(array(
	        'category_id' => 1,
	        'level_id' => 3,
	        'number_id' => 32,
	        'user_id' => 1,
	        'title' => "Comprendre la bande d’annonce d’un film français",
	        'description' => "Aimez-vous le septième art. Venez découvrir un auteur : Boris Vian... un film : \"L'écume des jours\"... une histoire : d'amour à la française. Affinez et testez votre compréhension orale.",
	        'content' => "<p>&laquo;&nbsp;<span style=\"color:#008000\"><em><strong>L&rsquo;&eacute;cume des jours</strong></em></span>&nbsp;&raquo; (2013) est un film fran&ccedil;ais r&eacute;alis&eacute; par Michel Gondry. C&rsquo;est une adaptation du roman culte de Boris Vian avec Romain Duris et Audrey Tautou dans les r&ocirc;les principaux.</p><p>Boris Vian a &eacute;crit ce livre en 1946 (&eacute;ditions Gallimard), mais l&rsquo;&oelig;uvre ne conna&icirc;t le succ&egrave;s que bien plus tard, apr&egrave;s la mort de l&rsquo;auteur.</p><p>La premi&egrave;re version cin&eacute;matographique du c&eacute;l&egrave;bre roman de Boris Vian a &eacute;t&eacute; tourn&eacute;e en 1968 par Charles Belmont.</p><p><span style=\"color:#696969\"><strong>Regardez avec attention la bande-annonce du film.</strong></span></p><p>&nbsp;</p><p><iframe frameborder=\"0\" height=\"270\" src=\"//www.dailymotion.com/embed/video/xy7f9s\" width=\"480\"></iframe><br /><a href=\"http://www.dailymotion.com/video/xy7f9s_l-ecume-des-jours-bande-annonce_shortfilms\" target=\"_blank\">L&#39;&eacute;cume des jours bande-annonce</a> <em>par <a href=\"http://www.dailymotion.com/sortiescinema\" target=\"_blank\">sortiescinema</a></em></p><p><strong>Un peu de vocabulaire&nbsp;:</strong></p><ul><li><span style=\"color:#FF8C00\"><strong>&Eacute;cume&nbsp;:</strong></span>&nbsp;mousse blanche qui se forme &agrave; la surface d&rsquo;un liquide.</li><li><span style=\"color:#FF8C00\"><strong>Culte&nbsp;: </strong></span>qui&nbsp;provoque l&rsquo;enthousiasme, l&rsquo;admiration ou la passion pour une personne ou une chose. Par exemple&nbsp;: un auteur culte ou un film culte.</li>	<li><span style=\"color:#FF8C00\"><strong>N&eacute;nuphar&nbsp;:</strong></span> lotus, plante aquatique aux feuilles rondes et aux fleurs roses qui sont sur l&rsquo;eau.</li><li><span style=\"color:#FF8C00\"><strong>Poumon&nbsp;:</strong></span> plac&eacute; dans la poitrine,&nbsp;organe&nbsp;qui sert &agrave; respirer.</li></ul>",
	        'active' => 0,
	        'created_at' => new DateTime,
	        'updated_at' => new DateTime
        ));
    }
}

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();
        Category::create(array(
                'name' => 'Grammaire',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Category::create(array(
                'name' => 'Compréhension',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Category::create(array(
                'name' => 'Vocabulaire',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Category::create(array(
                'name' => 'Français précoce',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
    }
}

class LevelTableSeeder extends Seeder {

    public function run()
    {
        DB::table('levels')->delete();
        Level::create(array(
                'name' => 'A1 - Débutant',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Level::create(array(
                'name' => 'A2 - Élémentaire',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Level::create(array(
                'name' => 'B1 - Autonome',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Level::create(array(
                'name' => 'B2 - Avancé',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
				Level::create(array(
                'name' => 'C1 - Expert',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
        ));
    }
}
