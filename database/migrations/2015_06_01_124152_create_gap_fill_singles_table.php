<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGapFillSinglesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gap_fill_singles', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');

				$table->integer('question_id');

				$table->string('word');

				$table->text('word_feedback');
				$table->integer('word_position')->default(1);

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gap_fill_singles');
	}

}
