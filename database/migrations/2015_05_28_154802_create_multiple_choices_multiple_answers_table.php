<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultipleChoicesMultipleAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('multiple_choices_multiple_answers', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');

			$table->integer('question_id');
			$table->text('content');
			$table->text('feedback');
			$table->boolean('correct');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('multiple_choices_multiple_answers');
	}

}
