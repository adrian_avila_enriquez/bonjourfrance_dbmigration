<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exercices', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');

			// $table->boolean('active');
			$table->integer('lesson_id');
		    $table->string('title');
			$table->text('content');
			$table->integer('order');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exercices');
	}

}
