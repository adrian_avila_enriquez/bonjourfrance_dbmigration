<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryClassificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_classifications', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');

				$table->integer('question_id');

				$table->text('content');
				$table->text('feedback');
				$table->string('words');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_classifications');
	}

}
