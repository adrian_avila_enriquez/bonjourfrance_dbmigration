<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociationTwoColumnsVideosTextsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('association_two_columns_videos_texts', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');

				$table->integer('question_id');

				$table->string('fixed_column');
				$table->text('video_url');
				$table->text('right_column');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('association_two_columns_videos_texts');
	}

}
