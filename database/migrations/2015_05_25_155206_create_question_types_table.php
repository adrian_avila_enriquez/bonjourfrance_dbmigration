<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('question_types', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');
			$table->integer('question_type_category_id');
			$table->string('system_name');
			$table->string('name');
			$table->text('instructions');
			$table->string('state');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('question_types');
	}

}
