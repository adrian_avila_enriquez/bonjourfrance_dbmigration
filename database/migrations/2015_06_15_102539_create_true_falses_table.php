<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrueFalsesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('true_falses', function($table)
		{
			$table->engine = 'InnoDB';

		    $table->increments('id');

				$table->integer('question_id');

				$table->text('content');
				$table->text('feedback');
				$table->string('answer');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('true_falses');
	}

}
