<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lessons', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    
		    $table->text('cover_picture');
		    $table->integer('category_id')->unsigned();
		    $table->foreign('category_id')->references('id')->on('categories')
			    ->onUpdate('cascade')
	      		->onDelete('cascade');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')
		    	->onUpdate('cascade')
      			->onDelete('cascade');

		    $table->integer('level_id')->unsigned();
		    $table->foreign('level_id')->references('id')->on('levels')
		    	->onUpdate('cascade')
      			->onDelete('cascade');

		    $table->integer('number_id');
			$table->boolean('active');
			$table->boolean('toggleStateAsked');
		    $table->string('title');
		    $table->text('description');
		    $table->text('content');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lessons');
	}

}
