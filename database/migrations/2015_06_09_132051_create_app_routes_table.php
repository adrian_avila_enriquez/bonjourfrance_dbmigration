<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppRoutesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app_routes', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');

			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('categories')
			    ->onUpdate('cascade')
	      		->onDelete('cascade');

			$table->integer('level_id')->unsigned();
			$table->foreign('level_id')->references('id')->on('levels')
		    	->onUpdate('cascade')
      			->onDelete('cascade');

			$table->integer('lesson_id')->unsigned();
			$table->foreign('lesson_id')->references('id')->on('lessons')
		    	->onUpdate('cascade')
      			->onDelete('cascade');

			$table->string('lang');
			$table->string('type');
			$table->string('public_url');
			$table->boolean('active');

		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app_routes');
	}

}
