<?php

function generateVideoIframe($video_url){
  if (preg_match('/youtu\.be/i', $video_url) || preg_match('/youtube\.com\/watch/i', $video_url)) {
    return generateYoutubeIframe($video_url);
  } elseif ( preg_match('/dailymotion/i', $video_url) ) {
    return generateDailymotionIframe($video_url);
  } else {
    return 'Url non-reconnue.';
  }
}

function generateYoutubeIframe($video_url){
  parse_str( parse_url( $video_url, PHP_URL_QUERY ), $vars );
  return '<iframe width="100%" height="150" src="https://www.youtube.com/embed/' . $vars['v'] . '" frameborder="0" allowfullscreen></iframe>';
}

function generateDailymotionIframe($video_url){
  $id = strtok(basename($video_url), '_');
  return '<iframe frameborder="0" width="100%" height="150" src="//www.dailymotion.com/embed/video/' . $id . '" allowfullscreen></iframe>';
}

function generateCategoryClassification($question_id, $choice_id, $words, $desactivated = null){
  $exploded_words = explode(',', $words);
  if( $desactivated ){
    foreach($exploded_words as $word){
      $result[] = '<a style="margin-bottom: 3px;" class="btn btn-lg btn-info">' . $word . '</a>';
    }
  }else{
    foreach($exploded_words as $word){
      $result[] = '<a data-questionid="' . $question_id . '" data-choiceid="' . $choice_id . '" style="margin-bottom: 3px;" class="word-category_classification-' . $choice_id . ' btn btn-lg btn-info">' . $word . '</a>';
    }
  }

  return implode(' ', $result);
}

function generateGapFillSingle($question_id, $original_text, $lines, $type = 'public'){
  $modified_text = $original_text;

  foreach($lines as $line){

    // echo '<hr />';
    // echo $line->word;

    $word = $line->word;
    $position = $line->word_position;
    $modified_text = $modified_text;

    $replace = createInput($question_id, $line, $original_text, $type);

    $current_word_position = 0;
    $pattern = "/$line->word\$/i";
    $temp_position = $position;
    $temp_position = $position;

    //echo ' : mot seul : ' . $temp_position;
    // echo '<br />';

    $text_array = explode(' ', $modified_text);

    foreach($text_array as $key => $row){

      /*
      echo '<hr />';
      echo $row;
      echo '<hr />';
      */

      if( preg_match($pattern, $row) ){

        $current_word_position++;

        /*
        echo $current_word_position;
        echo '<br />';
        echo $temp_position;
        */

        if( $current_word_position == $temp_position ){

          // echo ' : trouvé en position ' . $current_word_position . ' => ' . $temp_position . ' : ';

          $text_array[$key] = preg_replace($pattern, $replace, $text_array[$key]);
        }

      }
    }

    $modified_text = implode(' ', $text_array);

  }

  return $modified_text;

}

function createInput($question_id, $line, $text, $type){
  return '<input placeholder=">> Écrire ici" data-feedback="' . $line->word_feedback . '" class="correct-gap_fill_singles-' . $question_id . ' form-control input-sm" type="text" data-answer="' . $line->word . '" style="margin-left:5px; margin-right: 5px;width:auto !important;display: inline;" />';
}

function generateGapFillMultiple($question_id, $original_text, $lines, $type = 'public'){

  $modified_text = $original_text;

  foreach($lines as $line){

    // echo '<hr />';
    // echo $line->word;

    $word = $line->word;
    $position = $line->word_position;
    $modified_text = $modified_text;

    $replace = createSelect($question_id, $line, $original_text, $type);

    $current_word_position = 0;
    $pattern = "/$line->word\$/i";
    $temp_position = $position;
    $temp_position = $position;

    //echo ' : mot seul : ' . $temp_position;
    // echo '<br />';

    $text_array = explode(' ', $modified_text);

    foreach($text_array as $key => $row){

      /*
      echo '<hr />';
      echo $row;
      echo '<hr />';
      */

      if( preg_match($pattern, $row) ){

        $current_word_position++;

        /*
        echo $current_word_position;
        echo '<br />';
        echo $temp_position;
        */

        if( $current_word_position == $temp_position ){

          // echo ' : trouvé en position ' . $current_word_position . ' => ' . $temp_position . ' : ';

          $text_array[$key] = preg_replace($pattern, $replace, $text_array[$key]);
        }

      }
    }

    $modified_text = implode(' ', $text_array);

  }

  return $modified_text;

}

function createSelect($question_id, $line, $text, $type){

  if( $type == 'admin' ){
    $temp_options[] = '<option data-feedback="' . $line->word_feedback . '" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $line->word . '" data-correct="1">' . $line->word . ' (correcte)</option>';
  }else{
    $temp_options[] = '<option data-feedback="' . $line->word_feedback . '" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $line->word . '" data-correct="1">' . $line->word . '</option>';
  }

  if( $line->proposition_one != '' ){
    $temp_options[] = '<option data-feedback="' . $line->proposition_one_feedback . '" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $line->proposition_one . '" data-correct="0">' . $line->proposition_one . '</option>';
  }else{
    do{
      $splitted_text = explode(' ', $text);
      $max_length = sizeof($splitted_text) - 5;
      $fake_position = rand(0, $max_length);
      $fake_proposition_one = $splitted_text[$fake_position];
      $search  = array(',', '.', ':', ';', '!', '?', '(', ')', '"');
      $replace  = array('', '', '', '', '', '', '', '', '');
      $fake_proposition_one = str_replace($search, $replace, $fake_proposition_one);
    }while( $fake_proposition_one == $line->word );

    $temp_options[] = '<option data-feedback="" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $fake_proposition_one . '" data-correct="0">' . $fake_proposition_one . '</option>';
  }

  if( $line->proposition_two != '' ){
    $temp_options[] = '<option data-feedback="' . $line->proposition_two_feedback . '" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $line->proposition_two . '" data-correct="0">' . $line->proposition_two . '</option>';
  }else{

    do{
      $splitted_text = explode(' ', $text);
      $max_length = sizeof($splitted_text) - 5;
      $fake_position = rand(0, $max_length);
      $fake_proposition_two = $splitted_text[$fake_position];
      $search  = array(',', '.', ':', ';', '!', '?', '(', ')', '"');
      $replace  = array('', '', '', '', '', '', '', '', '');
      $fake_proposition_two = str_replace($search, $replace, $fake_proposition_two);
    }while( $fake_proposition_two == $line->word );

    $temp_options[] = '<option data-feedback="" class="correct-gap_fill_multiples-' . $question_id . '" data-choice="' . $fake_proposition_two . '" data-correct="0">' . $fake_proposition_two . '</option>';
  }

  shuffle($temp_options);
  array_unshift($temp_options, '<option data-correct="0">>> Choisir une proposition</option>');
  return '<select class="form-control" style="margin-left:5px; margin-right: 5px;width:auto !important;display: inline;">' . implode('', $temp_options) . '</select>';
}
