<?php

use \App\Models\AppRoute;
use \App\Models\Lesson;
use \App\Models\Category;
use \App\Models\User;
use \App\Models\Level;
use \App\Models\Exercice;
use \App\Models\Question;
use \App\Models\QuestionType;
use \App\Models\QuestionTypeCategory;
use \App\Models\Translation;

// Question types
use \App\Models\QuestionTypes\MultipleChoicesSingleAnswers;
use \App\Models\QuestionTypes\MultipleChoicesMultipleAnswers;
use \App\Models\QuestionTypes\AssociationTwoColumnsTextsTexts;
use \App\Models\QuestionTypes\AssociationTwoColumnsPicturesTexts;
use \App\Models\QuestionTypes\AssociationTwoColumnsVideosTexts;
use \App\Models\QuestionTypes\GapFillMultiples;
use \App\Models\QuestionTypes\GapFillSingles;
use \App\Models\QuestionTypes\TrueFalses;
use \App\Models\QuestionTypes\TrueFalseUnknowns;
use \App\Models\QuestionTypes\CategoryClassifications;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Private
Route::group(array('middleware' => 'auth', 'prefix' => 'admin'), function()
{

  Route::get('/test', ['as' => 'test', function(){
    return view('test_association_two_columns');
  }]);

  /********************************************
    QUESTIONS TYPES
  ********************************************/
    // => True / False
    Route::get('/checkLineRadioTrueFalses/{question_id}/{line_id}/{state}', ['as' => 'checkLineRadioTrueFalses', function($question_id, $line_id, $state){
      $questionLine = TrueFalses::findOrFail($line_id);
      $questionLine->answer = $state;
      $questionLine->save();
    }]);
    // => Multiple choices single answer
    Route::get('/checkLineRadio/{question_id}/{line_id}', ['as' => 'checkLineRadio', function($question_id, $line_id){
  		$question = Question::findOrFail($question_id);
      foreach($question->multiple_choices_single_answers as $questionLine){
        $questionLine->correct = false;
        $questionLine->save();
      }
      $questionLine = MultipleChoicesSingleAnswers::findOrFail($line_id);
      $questionLine->correct = true;
      $questionLine->save();
  	}]);
    // => Multiple choices multiple answers
    Route::get('/checkLineCheckbox/{question_id}/{line_id}/{state}', ['as' => 'checkLineCheckbox', function($question_id, $line_id, $state){
      $questionLine = MultipleChoicesMultipleAnswers::findOrFail($line_id);
      if($state == 'true'){
        $questionLine->correct = true;
      }else{
        $questionLine->correct = false;
      }
      $questionLine->save();
  	}]);
    // => Association two columns texts texts
    Route::get('/fixedColumns/{question_id}/{column}', ['as' => 'fixedColumns', function($question_id, $column){
      $question = Question::findOrFail($question_id);
      foreach($question->association_two_columns_texts_texts as $questionLine){
        $questionLine->fixed_column = $column;
        $questionLine->save();
      }
  	}]);
    // => Association two columns pictures texts
    Route::get('/fixedColumnsPictures/{question_id}/{column}', ['as' => 'fixedColumnsPictures', function($question_id, $column){
      $question = Question::findOrFail($question_id);
      foreach($question->association_two_columns_pictures_texts as $questionLine){
        $questionLine->fixed_column = $column;
        $questionLine->save();
      }
  	}]);

  /********************************************
    IMAGES + FILES
  ********************************************/
    // => Save a picture on Cloudinary
    Route::post('postImage', function(){
      if (Input::hasFile("file")) {
        $base_route = 'http://localhost/bonjour_de_france_l5/laravel/public';
        $config = array('directory_upload'=>'img/uploads');
        $file = Input::file("file");
        $file = (!method_exists($file, "getClientOriginalName")) ? $file['file'] : $file;
        $filename = $file->getClientOriginalName();
        $suffixe = "01";
        while (file_exists(public_path($config['directory_upload'])."/".$filename)) {
          $filename = $suffixe."_".$filename;
          $suffixe++;
          if ($suffixe < 10) {
            $suffixe = "0".$suffixe;
          }
        }
        if ($file->move(public_path($config['directory_upload']), $filename)) {
          \Cloudinary::config(array(
            "cloud_name" => "bdf",
            "api_key" => "492812336224234",
            "api_secret" => "M0mhJ6Uh5kXWdWBGBWUhp-ql9rw"
          ));
          $cloudinary_content = \Cloudinary\Uploader::upload(
            $config['directory_upload']."/".$filename,
            array("public_id" => "bonjour_de_france/" . $filename)
          );
          $remote_url = $cloudinary_content['url'];
          $return = [
            "filelink" => $remote_url
          ];
          echo json_encode($return);
        }
      }
    });

    // => Files collection
    Route::get('fileGallery.json', ['as' => 'fileGallery', function(){
      \Cloudinary::config(array(
        "cloud_name" => "bdf",
        "api_key" => "492812336224234",
        "api_secret" => "M0mhJ6Uh5kXWdWBGBWUhp-ql9rw"
      ));
      $api = new \Cloudinary\Api();
      $results = $api->resources(array("type" => "raw", "prefix" => "bonjour_de_france/"));
      $iterator = $results->getIterator();
      $json_result = array();
      foreach($iterator['resources'] as $key => $picture){
        $json_result[$key] = array(
          'title' => $picture['public_id'],
          'name' => $picture['public_id'],
          'link' => $picture['url'],
          'size' => $picture['bytes']
        );
      }
      return Response::json( $json_result );
    }]);

    // => Images gallery
    Route::get('imageGallery.json', ['as' => 'imageGallery', function(){
      \Cloudinary::config(array(
        "cloud_name" => "bdf",
        "api_key" => "492812336224234",
        "api_secret" => "M0mhJ6Uh5kXWdWBGBWUhp-ql9rw"
      ));
      $api = new \Cloudinary\Api();
      $results = $api->resources(array("type" => "upload", "prefix" => "bonjour_de_france/"));
      $iterator = $results->getIterator();
      $json_result = array();
      foreach($iterator['resources'] as $key => $picture){
        $json_result[$key] = array(
          'thumb' => 'http://res.cloudinary.com/bdf/image/upload/c_scale,w_100/v1430324306/' . $picture['public_id'] . '.jpg',
          'image' => $picture['url'],
          'title' => $picture['public_id']
        );
      }
      return Response::json( $json_result );
    }]);

  /********************************************
    REDACTOR
  ********************************************/
    // => Lesson
    Route::post('quickSaveLessonContent', ['as' => 'quickSaveLessonContent', function(){
      $lesson = Lesson::find($_POST['lesson_id']);
      $lesson->content = $_POST['lessonContent'];
      if( $lesson->save() ){
  			return Response::json( array('state'=>'ok') );
  		}
  	}]);
    // => Exercice
    Route::post('quickSaveExerciceContent', ['as' => 'quickSaveExerciceContent', function(){
      $exercice = Exercice::find($_POST['exercice_id']);
      $exercice->content = $_POST['exerciceContent'];
      if( $exercice->save() ){
  			return Response::json( array('state'=>'ok') );
  		}
  	}]);
    // => Question
    Route::post('quickSaveQuestionContent', ['as' => 'quickSaveQuestionContent', function(){
      $question = Question::find($_POST['question_id']);
      $question->content = $_POST['questionContent'];
      if( $question->save() ){
  			return Response::json( array('state'=>'ok') );
  		}
  	}]);

  /********************************************
    XEDITABLE
  ********************************************/
    // => Lesson edition
    Route::post('quickPostLesson', ['as' => 'quickPostLesson', function(){
      $inputs = Input::all();
      $lesson = Lesson::find($inputs['pk']);
      $lesson->$inputs['name'] = $inputs['value'];
      if( $lesson->save() ){
        return Response::json($lesson);
      }
    }]);

    // => Exercice edition
    Route::post('quickPostExercice', ['as' => 'quickPostExercice', function(){
  		$inputs = Input::all();
  		$exercice = Exercice::find($inputs['pk']);
  		$exercice->$inputs['name'] = $inputs['value'];
  		if( $exercice->save() ){
  			return Response::json($exercice);
  		}
  	}]);

    // => Question edition
    Route::post('quickPostQuestion', ['as' => 'quickPostQuestion', function(){
  		$inputs = Input::all();
  		$question = Question::find($inputs['pk']);
  		$question->$inputs['name'] = $inputs['value'];
  		if( $question->save() ){
  			return Response::json($question);
  		}
  	}]);

    // => Question line edition
  	Route::any('quickPostLine/{question_id}', ['as' => 'quickPostLine', function($question_id){

      $question = Question::findOrFail( $question_id );
      $model_name = '\App\Models\QuestionTypes\\' . ucfirst( camel_case( $question->type->system_name ) );

  		$inputs = Input::all();

  		$element = $model_name::find($inputs['pk']);
  		$element->$inputs['name'] = $inputs['value'];

  		if( $element->save() ){
  			return Response::json($element);
  		}
  	}]);

  /********************************************
    INDEX
  ********************************************/
    // => Lessons
    Route::get('/lessons', ['as' => 'lessons', function(){
      $lessons = Lesson::with('level')->with('category')->get();
      return view('admin.lessons.index')
        ->with('lessons', $lessons);
    }]);

  /********************************************
    SHOW
  ********************************************/
    // => Lesson
  	Route::get('/lesson/show/{id}', ['as' => 'lessonShow', function($id){
  		$lesson = Lesson::findOrFail($id);
  		return view('admin.lessons.show')
  			->with('lesson', $lesson);
  	}]);

  /********************************************
    EDIT
  ********************************************/
    // => Lesson
    Route::get('/lesson/edit/{id?}', ['as' => 'lessonEdit', function($id = null){
      $lesson = Lesson::findOrNew($id);
      if( $id == null ){
        $lesson->active = 0;
        $lesson->save();
        return redirect()->route('lessonEdit', [$lesson->id]);
      }
      $lesson->save();
      $levels = json_encode( Level::select(array('id as value','name as text'))->get()->toArray() );
      $categories = json_encode( Category::select(array('id as value','name as text'))->get()->toArray() );
      $question_types = QuestionTypeCategory::all();
      return view('admin.lessons.edit')
        ->with('lesson', $lesson)
        ->with('levels', $levels)
        ->with('question_types', $question_types)
        ->with('categories', $categories);
    }]);
    // => Question
    Route::get('/question/edit/{id}', ['as' => 'questionEdit', function($id){
      $lesson = Lesson::findOrFail( Input::get('lesson_id') );
      $question = Question::findOrNew($id);
      $question->save();
      return view('admin.questions.edit')
        ->with('question', $question)
        ->with('lesson', $lesson);
    }]);

  /********************************************
    ADD
  ********************************************/
    // => Exercice
    Route::get('/exercice/add/{lesson_id}', ['as' => 'addExercice', function($lesson_id){
      $lesson = Lesson::findOrFail( $lesson_id );
      $number_exercices = $lesson->exercices->count();
      $exercice = new Exercice;
      $exercice->order = $number_exercices + 1;
      $exercice->lesson_id = $lesson_id;
      $exercice->save();
      return redirect()->route('lessonEdit', [$lesson_id]);
    }]);
    // => Question
    Route::get('/question/add/{lesson_id}', ['as' => 'addQuestion', function($lesson_id){
      $exercice = Exercice::findOrFail( Input::get('exercice_id') );
      $number_questions = $exercice->questions->count();
      $question = new Question;
      $question->question_type_id = Input::get('question_type_id');
      $question->content = 'Brouillon n°' . ( $number_questions + 1 );
      $question->order = $number_questions + 1;
      $question->exercice_id = Input::get('exercice_id');
      $question->save();
      return redirect()->route('questionEdit', [$question->id, 'lesson_id'=>$lesson_id]);
    }]);
    // => Question line
    Route::get('/question/add/line/{question_id}', ['as' => 'addLine', function($question_id){
      $question_type = QuestionType::findOrFail( Input::get('question_type_id') );
      $table_name = $question_type->system_name;
      $model_name = '\App\Models\QuestionTypes\\' . ucfirst( camel_case( $question_type->system_name ) );
      $question = Question::findOrFail( $question_id );
      $number_lines = $question->{$table_name}->count();
      $element = new $model_name;
      $element->question_id = $question_id;
      $element->save();
      return redirect()->back()->withInput();
    }]);

  /********************************************
    REMOVE
  ********************************************/
    // => Lesson
  	Route::get('/lesson/remove/{id}', ['as' => 'lessonRemove', function($id){
  		$lesson = Lesson::findOrFail($id);
  		$lesson->delete();
  		return redirect()->route('lessons');
  	}]);
    // => Exercice
  	Route::get('/exercice/remove/{id}', ['as' => 'exerciceRemove', function($id){
  		$exercice = Exercice::findOrFail($id);
  		$exercice->delete();
  		return redirect()->route('lessonEdit', Input::get('lesson_id'));
  	}]);
    // => Question
  	Route::get('/question/remove/{id}', ['as' => 'questionRemove', function($id){
  		$question = Question::findOrFail($id);
  		$question->delete();
  		return redirect()->route('lessonEdit', Input::get('lesson_id'));
  	}]);
    // => Question line
  	Route::get('/question/remove/line/{id}', ['as' => 'lineRemove', function($id){
      $question = Question::findOrFail( Input::get('question_id') );
      $model_name = '\App\Models\QuestionTypes\\' . ucfirst( camel_case( $question->type->system_name ) );

  		$element = $model_name::findOrFail($id);
  		$element->delete();
      return redirect()->back()->withInput();
  	}]);

  /********************************************
    SORT
  ********************************************/
    // => Exercices and questions sorting
  	Route::post('sortElements', ['as' => 'sortElements', function(){
      $new_sort = Input::get('sort');
      foreach($new_sort as $exercice_key => $exercice_array){
        $exercice = Exercice::findOrFail( $exercice_array['id'] );
        $exercice->order = $exercice_key + 1;
        $exercice->save();
        // Check if the current exercice have childrens
        // If yes, change order + exercice_id
        if( isset( $exercice_array['children'] ) ){
          foreach($exercice_array['children'] as $question_key => $question){
            $question = Question::findOrFail( $question['id'] );
            $question->exercice_id = $exercice_array['id'];
            $question->order = $question_key + 1;
            $question->save();
            $question = null;
          }
        }
        $exercice = null;
      }
  	}]);

  /********************************************
    TOGGLE STATE
  ********************************************/
    // => Lesson
    Route::get('/lesson/state/{lesson_id}', ['as' => 'toggleLessonState', function($lesson_id){
      $lesson = Lesson::findOrFail( $lesson_id );
      $lesson->active = Input::get('active');
      $lesson->save();
      return redirect()->route('lessonEdit', [$lesson_id]);
    }]);

  /********************************************/

});

  Route::get('/home', ['middleware' => 'auth', 'as' => 'home', function(){
    return view('admin.home');
  }]);

// Public

  // SignOut
  Route::get('/signOut', ['as' => 'signOut', function(){
    Auth::logout();
    return Redirect::to( URL::to('/auth/login') );
  }]);


function newCategoryId($old_category_id){

  $new_category_id = -1;

  switch ($old_category_id) {
    case 1: // Grammaire ->  Grammaire
        $new_category_id = 1;
        break;
    case 2: // Compréhension -> Compréhension 
        $new_category_id = 2;
        break;
    case 3: // Vocabulaire -> Vocabulaire
        $new_category_id = 3;
        break;
    case 4: // Fiches Pédagogiques -> Fiches pédagogiques
        $new_category_id = 4;
        break;
    case 5: // Français des Affaires -> Français des affaires
        $new_category_id = 5;
        break;
    case 6: // Parler Français -> Parler français
        $new_category_id = 11;
        break;
    case 7: // Jeux -> Jeux pour apprendre le français
        $new_category_id = 8;
        break;
    case 8: // Expressions Idiomatiques -> Expressions idiomatiques
        $new_category_id = 9;
        break;
    case 11: // Civilisation -> Civilisation française
        $new_category_id = 10;
        break;
    case 15: // Préparation Delf -> Préparation DELF
        $new_category_id = 12;
        break;
    case 16: // Exercices de conjugaison -> Exercices de conjugaison
        $new_category_id = 7;
        break;
    case 17: // Destination Francophonie -> Destination francophonie
        $new_category_id = 13;
        break;
    case 19: // Formations pour professeurs -> Formation pour professeurs
        $new_category_id = 14;
        break;
  }

  /* NOT NEW_ID FOR:
      Français précoce
  */

  return $new_category_id;
}

function getIsoFormat_Lang($lang){

  $formated_lang = "unknown";

  switch($lang){
      case 'fr': $formated_lang = 'fr-FR';
      break;
      case 'en': $formated_lang = 'en-GB';
      break;
      case 'de': $formated_lang = 'de-DE';
      break;
      case 'pt': $formated_lang = 'pt-BR';
      break;
      case 'es': $formated_lang = 'es-ES';
      break;
      case 'ru': $formated_lang = 'ru-RU';
      break;
      case 'it': $formated_lang = 'it-IT';
      break;
      case 'sv': $formated_lang = 'sv-SV';
      break;
      case 'gr': $formated_lang = 'gr-GR';
      break;
      case 'cn': $formated_lang = 'cn-CN';
      break;
      case 'pl': $formated_lang = 'pl-PL';
      break;
      case 'tr': $formated_lang = 'tr-TR';
      break;
      case 'ch': $formated_lang = 'ch-CH';
      break;
    }

  return $formated_lang;
}

function newQuestionTypeId($old_id){

  $new_type_id = -1;

  switch ($old_id) {
    case 1: // Qcm à réponses multiples -> multiple_choices_single_answers
        $new_type_id = 2;
        break;
    case 2: // Qcm à réponse unique -> multiple_choices_multiple_answers
        $new_type_id = 1;
        break;
    case 3: // Associations à deux colonnes -> association_two_columns_texts_texts
            //                              -> association_two_columns_pictures_texts
        $new_type_id = 3;
        break;
    case 10: // Texte lacunaire-Réponses multiples -> gap_fill_multiples
        $new_type_id = 4;
        break;
    case 11: // Question Vrai ou Faux -> true_falses
        $new_type_id = 8;
        break;
    case 12: // Texte lacunaire-Réponse libre -> gap_fill_singles
        $new_type_id = 9;
        break;
    case 13: // Texte lacunaire (texte à trous) à réponse par vignettes -> 
        $new_type_id = 13;
        break;
    case 15: // Classification par catégorie -> category_classifications
        $new_type_id = 7;
        break;
    case 19: // Viseur FLE -> viseur_fles
        $new_type_id = 11;
        break;
    case 21: // Anagramme -> anagrammes
        $new_type_id = 13;
        break;
  }

  return $new_type_id;
}


/********************************************
    DATABASE 
********************************************/
  // Database Migration
  // Switch between databases specified by protected $connection in Models
Route::get('/database', ['as' => 'database', function(){

// Script is going to take a while...
set_time_limit(10000);

// Cloudinary COnfiguration
\Cloudinary::config(array( 
  "cloud_name" => "azurmedia", 
  "api_key" => "549771761412484", 
  "api_secret" => "KTTCB845U6H3QhFE8J_erq_zmW8" 
));

/** PAGES **/
// Retrieve data from table : 'pages'
$query_result = DB::select('select * from pages');
$all_pages = (array) $query_result; echo "</pre>";

echo "<pre>"; echo "Size of query result: ".count($all_pages);

for ($i=0; $i < count($all_pages); $i++) { 
  $page = (array) $all_pages[$i];

  // Skip the non activated pages
  if ($page['status'] != 'active')
    continue;

  // Save the retrieved attributes of a 'page'
  $page_id = $page['id'];
  $page_status = ($page['status'] == 'active' ? true : false);
  $page_lesson_instructions = $page['lecon_consigne'];
  $page_meta_description = $page['meta_description'];
  $page_url = $page['url'];
  $page_alias = $page['alias'];
  $page_title = $page['titre'];
  $page_image = $page['image'];
  $page_creator_id = $page['creator_id'];

  // Retrieve data from table : 'pages_categories'
  $query_result = DB::connection('mysql')->select('select * from categories_pages where page_id = ?', [$page_id]);
  $category_page_row = (array) $query_result[0];
  $page_category_id = $category_page_row['categorie_id'];

  // Transform the old category into the new category
  $new_page_category_id = newCategoryId($page_category_id);

  // Skip the non-existent categories
  if($new_page_category_id == -1)
    continue;

  // Retrieve data from table : 'pages_niveaux'
  $query_result = DB::connection('mysql')->select('select * from niveaux_pages where page_id = ?', [$page_id]);
  // We assign the level_id (if it exists)
  if ($query_result != null) {
      $niveau_page_row = (array) $query_result[0];
      $page_level_id = $niveau_page_row['niveau_id'];
  }

  // Retrieve data from table : 'numeros_pages'
  $query_result = DB::connection('mysql')->select('select * from numeros_pages where page_id = ?', [$page_id]);
  // Retrieve the numero_id (if it exists)
  if ($query_result != null) {
        $numero_page_row = (array) $query_result[0];
        $page_number_id = $numero_page_row['numero_id'];
        
        // Retrieve data from table : 'numeros'
        $query_result = DB::connection('mysql')->select('select * from numeros where id = ?', [$page_number_id]);
        // We assign the numero value (if it exists)
        if ($query_result != null){
          $number_row = (array) $query_result[0];
          $number = $number_row['numero'];
        }else
          $number = '';
  }

  // Find the file for the cover picture
  $image_relative_url = "http://www.bonjourdefrance.com/image/".$page_image;

  if (!file_exists($image_relative_url))
    $image_relative_url = "http://www.bonjourdefrance.com/image/thumbnails/exercice.jpg";

  // Upload the image to Cloudinary
  $cloudinary_upload = \Cloudinary\Uploader::upload($image_relative_url);

  // Transforms the old user_id to the new user_id in the new DB
  $new_user_id = getNewUserId($page_creator_id);

  // Skip the non-existent users
  if ($new_user_id == -1)
    continue;

  /* DATA STORE */ //Insert the Lesson (Category, User, Level must exist)                    
  /**/
  $category = Category::find($new_page_category_id);
  $user = User::find($new_user_id);
  $level = Level::find($page_level_id);
  
  $lesson = new Lesson(
    [
      'cover_picture' => $cloudinary_upload['public_id'],
      'number_id' => $number,
      'active' => $page_status,
      'toggleStateAsked' => false,
      'title' => $page_title,
      'title_md5' => md5($page_title),
      'description' => $page_meta_description,
      'description_md5' => md5($page_meta_description),
      'content' => $page_lesson_instructions,
      'content_md5' => md5($page_lesson_instructions)
    ]
  );
  // Stablish the relationships of the Lesson (if the entities exist)
  if ($category != null) 
    $lesson->category()->associate($category);

  if ($user != null) 
    $lesson->user()->associate($user); 
  
  if ($level != null)
    $lesson->level()->associate($level);
  
  // Save the lesson
  $lesson->save();
  echo "<pre>"; echo "Page: ".$page_id." migrated."; echo "</pre>";

  /* DATA STORE */ //Insert the AppRoute (Category, Level, Lesson must exist)                    
  /**/
  $app_route = new AppRoute(
    [
      'lang' => 'fr-FR',
      'type' => 'lesson',
      'public_url' => ($page['alias'] == '' ? $page_url : $page_alias),
      'active' => $page_status
    ]
  );
  // Stablish the relationships of the AppRoute (if the entities exist) and we save it
  if ($category != null)
    $app_route->category()->associate($category);
  
  if ($level != null)
    $app_route->level()->associate($level);

  // Save the app_route
  $app_route->lesson()->associate($lesson);
  $app_route->save();
  /**/

  // Retrieve data from table : 'traductions'
  $query_result = DB::connection('mysql')->select('select * from traductions where id_page = ?', [$page_id]);
  // Retrieve the traductions (if they exist)
  if ($query_result != NULL) {
    $traductions = (array) $query_result;

    for ($n=0; $n < count($traductions); $n++) {
      $traduction = (array) $traductions[$n];
      $traduction_lang = $traduction['lang'];
      $traduction_title = $traduction['title'];
      $traduction_value = $traduction['value'];
      $traduction_lecon_instructions = $traduction['lecon_consigne'];

      /* DATA STORE */ // Insert the Translation (Lesson, User, Validator must exist)                  
      /**/
      $user = User::find(1); // 
      $validator = User::find(1);
      
      $translation = new Translation(
        [
          'lang' => getIsoFormat_Lang($traduction_lang),
          'title' => $traduction_title,
          'title_md5' => md5($traduction_title),
          'description' => $traduction_value,
          'description_md5' => md5($traduction_value),
          'lesson' => $traduction_lecon_instructions,
          'lesson_md5' => md5($traduction_lecon_instructions),
          'state' => 1
        ]
      );
      // We stablish the relationships of the Translation and we save it
      $translation->lesson()->associate($lesson);
      $translation->user()->associate($user);
      $translation->user()->associate($validator);
      $translation->save();
      /**/
    }
  } // end of translations section

  /** EXERCISES **/
  // Retrieve data from table : 'pages_exercices'
  $query_result = DB::connection('mysql')->select('select * from exercices_pages where page_id = ?', [$page_id]);
  // Retrieve the EXERCISES (if they exist)
  if ($query_result != NULL) {
    $page_exercice = (array) $query_result;

    // Loop for every exercise in a page
    for ($j=0; $j < count($page_exercice); $j++) {
      $exercice_page_row = (array) $page_exercice[$j];
      $exercice_id = $exercice_page_row['exercice_id'];

      // Retrieve data from table : 'exercices'
      $query_result = DB::connection('mysql')->select('select * from exercices where id = ?', [$exercice_id]);

      // Retrieve exercises attributes (if they exist)
      if ($query_result != NULL) {
        $exercice = (array) $query_result[0];

      // Skip the non-activated exercises 
      if ($exercice['status'] != 'active')
          continue;

        // Save the retrieved attributes of an 'exercise'
        $exercice_id = $exercice['id'];
        $exercice_status = $exercice['status'];
        $exercice_title = $exercice['titre'];
        $exercice_instructions = $exercice['consigne'];
        $exercice_video_audio = $exercice['video_audio'];
        $exercice_creator_id = $exercice['creator_id'];

        // Remove the exercise title if it is something like "Exercise 1" 
        $pos = strpos($exercice_title, "xercice"); 
        if ($pos === 1)
          $exercice_title = '';
        
        /* DATA STORE */ //Create the Exercise
        /**/
        $exercice = new Exercice(
          [
            'title' => $exercice_title,
            'instructions' => $exercice_instructions,
            'content' => $exercice_video_audio,
            'order' => $j+1 // Loop index 
          ]
        );
        //Insert the Exercise related to the correspondant Lesson
        $exercice->lesson()->associate($lesson);
        $exercice->save();
        /**/

        // Retrieve data from table : 'exercices_questions'
        $query_result = DB::connection('mysql')->select('select * from exercices_questions where exercice_id = ?', [$exercice_id]);

        /** QUESTIONS **/
        if ($query_result != NULL) {
          $exercice_question = (array) $query_result;

          // Loop for every question in an exercise
          for ($k=0; $k < count($exercice_question); $k++) {
            $question_exercice_row = (array) $exercice_question[$k];

            $question_id = $question_exercice_row['question_id'];
            
            // Retrieve data from table : 'questions'
            $query_result = DB::connection('mysql')->select('select * from questions where id = ?', [$question_id]);
            
            // Retrieve question attributes (if they exist)
             if ($query_result != NULL) {
                $question = (array) $query_result[0];

                // Skip the non-activated questions
                if ($question['status'] != 'active')
                    continue;

                // Extract the attributes from a 'question'
                $question_id = $question['id'];
                $question_status = $question['status'];
                $question_title = $question['libelle'];    
                $question_creator_id = $question['creator_id'];

                // Retrieve data from table : 'question_types'
                $query_result = DB::connection('mysql')->select('select * from questions_types where question_id = ?', [$question_id]);

                if ($query_result != NULL) {
                  // Retrieve question_type attributes (if they exist)
                  $question_type = json_decode(json_encode($query_result[0]),true);
                  $question_type_id = $question_type['id'];       
                  $question_type_question_id = $question_type['question_id'];
                  $question_type_type_id = $question_type['type_id'];

                  // Retrieve data from table : 'types'
                  $query_result = DB::connection('mysql')->select('select * from types where id = ?', [$question_type_type_id]);
                  // Retrieve type attributes (if they exist)
                  if ($query_result != NULL) {
                    $type = json_decode(json_encode($query_result[0]),true);
                    
                    // Skip the non-activated types
                    if ($type['status'] != 'active')
                        continue;

                    // Extract the attributes from a 'type'
                    $type_id = $type['id'];
                    $type_status = $type['status'];

                    // Transforms the old question_type_id to the new question_type_id in the new DB
                    $new_question_type = newQuestionTypeId($type_id);
                    
                    // Skip the non-existent question types
                    if ($new_question_type == -1)
                      continue;

                    // Decode field 'proposition'
                    $question_propostion = (array)$question['proposition'];
                    $proposition_json = $question_propostion[0];
                    $proposition_r = json_decode($proposition_json, true);

                    /* DATA STORE */ //Create the Question (QuestionType must exist)
                    /**/
                    $question_type = QuestionType::find(newQuestionTypeId($type_id));
                    $question = new Question(
                      [
                        'content' => $question_title,
                        'order' => $k+1 // Loop index
                      ]
                    );
                    // Stablish the relation with QuestionType
                    $question->type()->associate($question_type);
                    //Insert the Question related to the correspondant Exercice
                    $question->exercice()->associate($exercice);
                    $question->save();
                    /**/

                    $question = storeSpecificQuestion($type_id, $question, $proposition_r);
                  }  
                }
              }
          }
        } // end of questions section

      }
    }
  } // end of exercises section
  
} // End for pages section [main loop]

return "1 : END OF DATABASE [LESSONS MIGRATIONS] SCRIPT [SUCCESS]";

}]);

function getNewUserId($old_user_id){

  $new_user_id = -1;

  // Retrieve data from table : 'users' in the old DB
  $query_result = DB::connection('mysql')->select('select email from users where id = ?', [$old_user_id]);

  if ($query_result != null){
      $emails = (array) $query_result[0];
      $email = $emails['email'];

      // Retrieve data from table : 'users' in the new DB
      $query_result = DB::connection('mysql2')->select('select id from users where email = ?', [$email]);

      if ($query_result != null) {
        $user = (array)$query_result[0];
        $new_user_id = $user['id'];
      }
  }

  return $new_user_id;
}


Route::get('/migrate_users', ['as' => 'migrate_users', function(){

  set_time_limit(500);

  // Cloudinary COnfiguration
  \Cloudinary::config(array( 
    "cloud_name" => "azurlingua", 
    "api_key" => "834988564856631", 
    "api_secret" => "cyjnnNaHvWQe3IBHedZIUd0ez_U" 
  ));

  // Retrieve data from table : 'users' 
  $query_result = DB::connection('mysql')->select('select * from users');
  $users = (array) $query_result;
    
  // Loop all users
  for ($i=0; $i < count($users); $i++) {
      $user_row = (array) $users[$i];
      $user_id = $user_row['id'];
      $user_username = $user_row['username'];
      $user_email = $user_row['email'];
      $user_activated = $user_row['activated'];
      $user_banned = $user_row['banned'];

      // Skip the non activated / banned users 
      if ($user_activated == 0 and $user_banned == 1)
        continue;
      
      // Retrieve data from table : 'users' 
      $query_result = DB::connection('mysql')->select('select * from user_profiles where user_id = ?', [$user_id]);
      
      if ($query_result != NULL) {
        $profiles = (array) $query_result;
        $profile_row = (array) $profiles[0];

        $profile_nickname = $profile_row['nickname'];
        $profile_function = $profile_row['function'];
        $profile_description = $profile_row['description'];
        $profile_picture = $profile_row['picture'];
        $profile_cloudinary_id = $profile_row['cloudinary_id'];
        $profile_website_name = $profile_row['website_name'];
        $profile_scoopit_link = $profile_row['scoopit_link'];
        $profile_twitter_link = $profile_row['twitter_link'];
        $profile_google_link = $profile_row['google_link'];
        $profile_country = $profile_row['country'];
        $profile_traductor = $profile_row['traductor'];
        $profile_blog = $profile_row['blog'];
        $profile_blog_name = $profile_row['blog_name'];
        $profile_blog_username = $profile_row['blog_username'];
        $profile_blog_password = $profile_row['blog_password'];

        /* DATA STORE */ //Create the Question (QuestionType must exist)
        /**/
        $user = new User(
                    [
                      'name' => $profile_website_name,
                      'email' => $user_email,
                      'password' => Hash::make($profile_nickname),
                      'lang' => '',
                      'description' => $profile_description,
                      'country' => $profile_country,
                      'function' => $profile_function,
                      'picture' => $profile_cloudinary_id,
                      'scoop_it' => $profile_scoopit_link,
                      'twitter' => $profile_twitter_link,
                      'google' => $profile_google_link, 
                      'isAdmin' => false,
                      'isTranslationsModerator' => false
                    ]
        );
        $user->save();
        /**/
    }
    else
      "No result retrieved from table 'user_profiles'"; 
  }

  return "<pre><pre>"."END of DATABASE USERS script";

}]);






function storeSpecificQuestion($type_id, $question, $proposition_r){

  // 'proposition' structure is different for every question type
  switch ($type_id) {
    case 1: // Qcm à réponses multiples
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $choices = (array)$proposition_r['choices'];

      for ($i = 0; $i < count($choices) ; $i++) {
        $option = $choices[$i];
        
        /* DATA STORE */ //Create the specific question object
        /**/
        $specific_question = new MultipleChoicesMultipleAnswers(
          [
            'content' => $option['choice'],
            'feedback' => '', // NO feedback
            'correct' => $option['correct']
          ]
        );
        //Insert the specific question related to the correspondant Question
        $specific_question->question()->associate($question);
        $specific_question->save();
        /**/
      }
      break;
  case 2: // Qcm à réponse unique
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $choices = (array)$proposition_r['choices'];

      for ($i = 0; $i < count($choices) ; $i++) {
        $option = $choices[$i];

        /* DATA STORE */ //Create the specific question object
        /**/
        $specific_question = new MultipleChoicesSingleAnswers(
          [
            'content' => $option['choice'],
            'feedback' => '', // No feedback
            'correct' => $option['correct']
          ]
        );
        //Insert the specific question related to the correspondant Question
        $specific_question->question()->associate($question);
        $specific_question->save();
        /**/
      }
      break;
  case 3: // Associations à deux colonnes
          // Association_two_columns_pictures_texts [789]
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $left_column = $proposition_r['columns']['leftColumn'];
      $right_column = $proposition_r['columns']['rightColumn'];
   
      for ($i = 0; $i < count($left_column) ; $i++) {
        $l_row = $left_column[$i];
        $r_row = $right_column[$i];

        // We check if there is a field 'img : 1'
        if (isset($proposition_r['img'])) {
          
          // question_type is : association_two_columns_pictures_texts
          $question->question_type_id = 5;
          $question->save(); // we update the question_type_id

          // Upload the image to Cloudinary
          $picture_relative_url = "http://www.bonjourdefrance.com/image/".$l_row['name'];

          if (!file_exists($picture_relative_url))
            $image_relative_url = "http://www.bonjourdefrance.com/image/thumbnails/exercice.jpg";

          $cloudinary_upload = \Cloudinary\Uploader::upload($picture_relative_url);

          /* DATA STORE */ //Create the specific question object
          /**/
          $specific_question = new AssociationTwoColumnsPicturesTexts(
            [
              'fixed_column' => 'left',
              'picture_url' => $cloudinary_upload['public_id'],
              'right_column' => $r_row['name']
            ]
          );
          //Insert the specific question related to the correspondant Question
          $specific_question->question()->associate($question);
          $specific_question->save();
          /**/
        }else{
          /* DATA STORE */ //Create the specific question object
          /**/
          $specific_question = new AssociationTwoColumnsTextsTexts(
            [
              'fixed_column' => 'left',
              'left_column' => $l_row['name'],
              'right_column' => $r_row['name']
            ]
          );
          //Insert the specific question related to the correspondant Question
          $specific_question->question()->associate($question);
          $specific_question->save();
          /**/
        }
      }
      break;
  case 10: // Texte lacunaire - Réponses multiples
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $text = (isset($proposition_r['originalText']) ? $proposition_r['originalText'] : $proposition_r['text']);
      $word; $gap; $gap_index = 0;
      
      // Extract the options from old text format
      preg_match_all('/{([^}]*)}/', $text, $matches);

      // Separate the options
      foreach ($matches[1] as $key => $val){
        $propos = array();
        $options = explode(',',$val);

        // Select the current gap
        $gap = $matches[0][$gap_index];

        for ($z=0; $z < count($options); $z++) { 
          $pos = strpos($options[$z], '[');
    
          // Identify the correct option and save it like $word
          if ($pos !== false) 
              $word = substr($options[$z], 1, -1);
          else
            array_push($propos, $options[$z]);// Save the other options like propositions
        }

        // Replace the options with the word (clean text)
        $text = str_replace($gap, $word, $text);
        /* DATA STORE */ //Create the specific question object
        /**/
        // Update the content of the question with cleaned text
        $question->content = $text;
        $question->save();

        $specific_question = new GapFillMultiples(
          [
            'word' => $word,
            'word_feedback' => '',
            'word_position' => 1, 
            'proposition_one' => (count($propos) > 0 ? $propos[0] : ''),
            'proposition_one_feedback'=> '',
            'proposition_two' => (count($propos) > 1 ? $propos[1] : ''),
            'proposition_two_feedback' => ''
          ]
        );
        //Insert the specific question related to the correspondant Question
        $specific_question->question()->associate($question);
        $specific_question->save();
        /**/
        $gap_index++;
      }
      break;
  case 11: // Question Vrai ou Faux
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $answer = $proposition_r['answer']; 

      /* DATA STORE */ //Create the specific question object
      /**/
      $question->content = '';
      $question->save(); 

      $specific_question = new TrueFalses(
        [
          'content' => $instructions,
          'feedback' => '',
          'answer' => ($proposition_r['answer'] == 'correct' ? true : false),
        ]
      );
      //Insert the specific question related to the correspondant Question
      $specific_question->question()->associate($question);
      $specific_question->save();
      /**/
      break;
  case 12: // Texte lacunaire - Réponse libre
      $instructions = $proposition_r['instructions'];
      $scoring = $proposition_r['scoring'];
      $original_text = $proposition_r['originalText'];
      $modified_text = $proposition_r['modifiedText'];
      $word_set = $proposition_r['words'];

      /* DATA STORE */ //Create the specific question object
      /**/
      // Update the content of the question
      $question->content = $original_text;
      $question->save();
      
      for ($i = 0; $i < count($word_set) ; $i++) {
          $word = $word_set[$i];

          $specific_question = new GapFillSingles(
            [
              'word' => $word['word'],
              'word_feedback' => '',
              'word_position' => 1 // <- Reme,ber to check if it is the first appearance
            ]
          );
          //Insert the specific question related to the correspondant Question
          $specific_question->question()->associate($question);
          $specific_question->save();
      }
      /**/
      break;
  case 15: // Classification par catégorie
      $categories = $proposition_r['proposition']['categories']; // <<-- Test this with page_id = 475, 94

      for ($i = 0; $i < count($categories) ; $i++) {
        $category_row = $categories[$i];
        $category_title = $category_row['title'];
        $words_set = $category_row['words'];
        $words = "";

        for ($j = 0; $j < count($words_set) ; $j++){
          $word = $words_set[$j];

          if ($j == count($words_set)-1)
            $words = $words.$word;
          else
            $words = $words.$word.", ";
        }

        /* DATA STORE */ //Create the specific question object
        /**/
        $specific_question = new CategoryClassifications(
          [
            'content' => $category_title,
            'feedback' => '',
            'words' => $words
          ]
        );
        //Insert the specific question related to the correspondant Question
        $specific_question = $question->category_classifications()->save($specific_question);
        /**/
      }
      break;
  case 16: // Rallye
      $title = $proposition_r['title'];
      $question_header = $proposition_r['question_header'];
      $question_r = $proposition_r['questions'];
      $questions = array(); /**/
      $answers = array(); /* I dont understand the indicators in answers array*/ 

      for ($i = 0; $i < count($question_r) ; $i++) {
          $question_row = $question_r[$i];
          $questions[$i] = $question_row['question'];
          $answers[$i] = $question_row['answer'];
      }
      // The indexes of the questions and answers always match
      break; 
  case 19: // Viseur FLE
      $sets = $proposition_r['sets'];
      $id_name_r = array(); /**/
      $questions = array(); /**/
      $answers = array(); /**/
      $correct_index = array();

      for ($i = 0; $i < count($sets) ; $i++) {
          $set = $sets[$i];

          $id_name_r[$i] = $set['idName'];
          $questions[$i] = $set['question'];
          $answer_r = $set['answersArray'];
          $answers[$i] = array();
          
          for ($j = 0; $j < 5 ; $j++) {
              $answer = $answer_r[$j];
              $answers[$i][$j] = $answer['name'];
              
              if ($answer['correct'] == 'on'){
                  $correct_index[$i] = $j;
              }         
          }
      }

      /* DATA STORE */ //Create the specific question object
      /*
      $specific_question = new ViseurFles(
        [
          'name' => $instructions
        ]
      );
      //Insert the specific question related to the correspondant Question
      $specific_question = $question->viseur_fles()->save($specific_question);
      */

      break;
  case 21: // Anagramme
        //echo "\n -> A question of type 21";
        $word_set = $proposition_r['words'];
        $words = array();
        $sentences = array();
        for ($i = 0; $i < count($word_set) ; $i++) {
            $word = $word_set[$i];
            $words[$i] = $word['word'];
            $sentences[$i] = $word['sentence'];
        }

        /* DATA STORE */ //Create the specific question object
        /*
        $specific_question = new Anagrammes(
          [
            'name' => $instructions
          ]
        );
        //Insert the specific question related to the correspondant Question
        $specific_question = $question->anagrammes()->save($specific_question);
        */

        break;

  }  // END of switch case

  return $question;
}





  // Show one lesson
  Route::get('/lesson/{id}', ['as' => 'lesson', function($id){

  	$lesson = Lesson::findOrFail($id);

  	return view('lessons.show')
  		->with('lesson', $lesson);
  }]);

  Route::controllers([
  	'auth' => 'Auth\AuthController',
  	'password' => 'Auth\PasswordController',
  ]);

  // Match all routes
  Route::any('{path}', ['as' => 'page', function($path){

    Session::put('en-GB_url', 'http://127.0.0.1/bonjour_de_france_l5/laravel/public/index.php/');
    Session::put('fr-FR_url', 'http://localhost/bonjour_de_france_l5/laravel/public/index.php/');

    switch ( $_SERVER['SERVER_NAME'] ) {

      case '127.0.0.1':
        App::setLocale('en-GB');
      break;
      case 'localhost':
        App::setLocale('fr-FR');
      break;

      // OTHERWISE
      default:
        exit();
      break;

    }

    $route_count = AppRoute::where('public_url', '=', $path)->where('lang', '=', App::getLocale())->count();
    if( $route_count ){
      $route = AppRoute::where('public_url', '=', $path)->where('lang', '=', App::getLocale())->first();
      if( $route->lang == App::getLocale() ){

        switch($route->type){
          case 'home' :
            $routes = AppRoute::where('type', '=', 'choose_level')->where('lang', '=', App::getLocale())->get();
            return View::make('pages.home')
              ->with('routes', $routes);
            break;
          case 'choose_level' :
            $lessons = Lesson::where('category_id', '=', $route->category_id)->get();
            return View::make('pages.choose_level')
              ->with('lessons', $lessons);
            break;
          case 'choose_lesson' :
            $lessons = Lesson::where('category_id', '=', $route->category_id)->get();
            return View::make('pages.choose_lesson')
              ->with('lessons', $lessons);
            break;
          case 'lesson' :
            if( App::getLocale() == 'fr-FR' ){
              $lesson = Lesson::findOrFail( $route->lesson_id );
              return view('lessons.show')
                ->with('lesson', $lesson);
            }else{
              // echo 'Translated lesson';
            }
            break;
        }
      }
    }else{
      abort(404);
    }
  }])->where('path', '.*');

  // Log every SQL
  Event::listen('illuminate.query', function($query){
      //var_dump($query);
  });



 
