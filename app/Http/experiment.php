

/********************************************
    DATABASE
********************************************/
  // Database Migration

  Route::get('/database', ['as' => 'database', function(){
  
  //$question_types = DB::select('select * from questions_types');
  $question_types = DB::select('select * from questions_types where id = ?', [173]);

  foreach ($question_types as $question_type) {
    
    // Retrieve data from table : 'question_types'
    $question_type = (array) $question_type;
    $question_type_id = $question_type['id'];
    $question_type_question_id = $question_type['question_id'];
    $question_type_type_id = $question_type['type_id'];

    // Retrieve data from table : 'types'
    $type = DB::select('select * from types where id = ?', [$question_type_type_id]);

    if ($type != NULL) {

      $type = (array) $type[0];
      $type_id = $type['id'];
      $type_status = $type['status'];
      $type_libelle = $type['libelle'];
      $type_description = $type['description'];

      // Retrieve data from table : 'questions'
      $question = DB::select('select * from questions where id = ?', [$question_type_question_id]);

      if ($question != NULL) {

        $question = (array) $question[0];
        $question_id = $question['id'];
        $question_status = $question['status'];
        $question_libelle = $question['libelle'];    
        $question_nombre_points = $question['nombre_points'];
        $question_creator_id = $question['creator_id'];

        // Decode field 'proposition'
        $question_propostion = (array)$question['proposition'];
        $proposition_json = $question_propostion[0];
        $proposition_r = json_decode($proposition_json, true);
        echo '<pre>'; print_r($proposition_r); echo '</pre>';

        // 'proposition' structure is different for every questiontype
        switch ($type_id) {
        case 1:
            // Qcm à réponses multiples
            break;
        case 2:
            // Qcm à réponse unique
            $instructions = $proposition_r['instructions'];
            $scoring = $proposition_r['scoring'];
            $choices = (array)$proposition_r['choices'];

            $options = array();
            $answer_index;

            for ($i = 0; $i <= 2 ; $i++) {
              $option = $choices[$i];
              if ($option['correct'] == '1')
                  $answer_index = $i;
              
              $options[$i] = $option['choice'];
            }
            break;
        case 3:
            // Associations à deux colonnes
            break;
        case 4:
            // Association à trois colonnes
            break;
        case 5:
            // Mise en ordre
            break;
        case 6:
            // Image avec marquage et champ de réponse libre
            break;
        case 7:
            // Image avec marquage
            break;
        case 8:
            // Classement dans deux classeurs
            break;
        case 9:
            // Classement dans trois classeurs
            break;
        case 10:
            // Texte lacunaire - Réponses multiples
            break;
        case 11:
            // Question Vrai ou Faux
            $instructions = $proposition_r['instructions'];
            $scoring = $proposition_r['scoring'];
            $answer = $proposition_r['answer'];       
            break;
        }
      }
      // No question was retrived
      echo "No question was retrived"; // comment this line
    }
    // Not type was retrieved
    echo "No type was retrived";
  }
  
  return "\n\nDATABASE is now fully migrated";

}]);

/********************************************