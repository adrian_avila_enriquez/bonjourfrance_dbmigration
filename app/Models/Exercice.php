<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercice extends Model {

	use SoftDeletes;

	/**
	 * The database used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'exercices';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'instructions', 'content', 'order'];

	public function lesson(){
		return $this->belongsTo('App\Models\Lesson', 'lesson_id');
	}

	public function questions(){
		return $this->hasMany('App\Models\Question')->orderBy('order')->with('type');
	}

}
