<?php namespace App\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;

class TrueFalseUnknowns extends Model {

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'true_false_unknowns';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

}
