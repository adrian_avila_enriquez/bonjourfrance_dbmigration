<?php namespace App\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryClassifications extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category_classifications';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['content', 'feedback', 'words'];

}
