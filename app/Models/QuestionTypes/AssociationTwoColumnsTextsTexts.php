<?php namespace App\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociationTwoColumnsTextsTexts extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'association_two_columns_texts_texts';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['fixed_column', 'left_column', 'right_column'];

	public function question(){
		return $this->belongsTo('App\Models\Question', 'question_id');
	}

}
