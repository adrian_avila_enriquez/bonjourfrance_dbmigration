<?php namespace App\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GapFillMultiples extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gap_fill_multiples';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['word', 'word_feedback', 'word_postion', 'proposition_one', 'proposition_one_feedback', 'proposition_two', 'proposition_two_feedback'];

	public function question(){
		return $this->belongsTo('App\Models\Question', 'question_id');
	}

}
