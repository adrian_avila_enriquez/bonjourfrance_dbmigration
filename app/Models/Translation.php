<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use \App\Models\AppRoute;
use \App\Models\Lesson;

class Translation extends Model {

	use SoftDeletes;

	/**
	 * The database used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'translations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['lang', 'title','title_md5', 'description', 'description_md5', 'lesson', 'lesson_md5','state'];

	public function getUrl($lang, $lesson_id){
		$lesson = Lesson::where('id', '=', $lesson_id)->firstOrFail();
		if( $lesson->level_id == 1 ){
			$route = AppRoute::where('lang', '=', $lang)->where('lesson_id', '=', $lesson_id)->firstOrFail();
			return $route->public_url;
		}else{
			return '#';
		}
	}

	public function lesson(){
		return $this->belongsTo('App\Models\Lesson', 'lesson_id');
	}

	public function user(){
		return $this->belongsTo('App\Models\User', 'user_id');
	}

}
