<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionType extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database used by the model.
	 *
	 * @var string
	 */
	protected $table = 'question_types';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['system_name', 'name', 'instructions', 'state'];

	public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

}
