<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model {

	use SoftDeletes;

	/**
	 * The database used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'questions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['content', 'order'];

	public function lesson(){
		return $this->belongsTo('App\Models\Lesson', 'lesson_id');
	}

	public function exercice(){
		return $this->belongsTo('App\Models\Exercice', 'exercice_id');
	}

	public function type(){
		return $this->belongsTo('App\Models\QuestionType', 'question_type_id');
	}

  	public function state(){
		return $this->belongsTo('App\Models\State');
	}

	public function multiple_choices_single_answers(){
		return $this->hasMany('App\Models\QuestionTypes\MultipleChoicesSingleAnswers');
	}

	public function multiple_choices_multiple_answers(){
		return $this->hasMany('App\Models\QuestionTypes\MultipleChoicesMultipleAnswers');
	}

	public function association_two_columns_texts_texts(){
		return $this->hasMany('App\Models\QuestionTypes\AssociationTwoColumnsTextsTexts');
	}

	public function association_two_columns_pictures_texts(){
		return $this->hasMany('App\Models\QuestionTypes\AssociationTwoColumnsPicturesTexts');
	}

	public function association_two_columns_videos_texts(){
		return $this->hasMany('App\Models\QuestionTypes\AssociationTwoColumnsVideosTexts');
	}

	public function gap_fill_multiples(){
		return $this->hasMany('App\Models\QuestionTypes\GapFillMultiples');
	}

	public function gap_fill_singles(){
		return $this->hasMany('App\Models\QuestionTypes\GapFillSingles');
	}

	public function true_false_unknowns(){
		return $this->hasMany('App\Models\QuestionTypes\TrueFalseUnknowns');
	}

	public function true_falses(){
		return $this->hasMany('App\Models\QuestionTypes\TrueFalses');
	}

	public function category_classifications(){
		return $this->hasMany('App\Models\QuestionTypes\CategoryClassifications');
	}

}
