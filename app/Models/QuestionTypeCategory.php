<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionTypeCategory extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'question_type_categories';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function question_types(){
		return $this->hasMany('App\Models\QuestionType');
	}

}
