<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppRoute extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'app_routes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['lang',
							'type',
							'public_url',
							'active'
							];


	public function category(){
		return $this->belongsTo('App\Models\Category', 'category_id');
	}

	public function level(){
		return $this->belongsTo('App\Models\Level', 'level_id');
	}

	public function lesson(){
		return $this->belongsTo('App\Models\User', 'lesson_id');
	}

}
