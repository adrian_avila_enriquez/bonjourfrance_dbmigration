<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \App\Models\AppRoute;
use \App\Models\Translation;

class Lesson extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'lessons';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [	'cover_picture',
							'number_id',
							'active',
							'toggleStateAsked',
							'title',
							'title_md5',
							'description',
							'description_md5',
							'content',
							'content_md5'
							];

	public function category(){
		return $this->belongsTo('App\Models\Category', 'category_id');
	}

	public function user(){
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	public function level(){
		return $this->belongsTo('App\Models\Level', 'level_id');
	}

	public function exercices(){
		return $this->hasMany('App\Models\Exercice')->orderBy('order');
	}

}
