<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function lessons()
    {
        return $this->hasMany('App\Models\Lesson');
    }

    public function approutes()
    {
        return $this->hasMany('App\Models\AppRoute');
    }

	public function getRouteAttribute($lang){
		$route = AppRoute::where('type', '=', 'choose_level')->where('category_id', '=', $this->id)->where('lang', '=', $lang)->first();
		return $route->public_url;
	}

}


