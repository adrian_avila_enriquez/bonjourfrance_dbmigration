<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model {

	use SoftDeletes;

	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'levels';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function lessons()
    {
        return $this->hasMany('App\Models\Lesson');
    }

    public function approutes()
    {
        return $this->hasMany('App\Models\AppRoute');
    }

}
